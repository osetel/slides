---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# EC 33 "Culture numérique et apprentissage"
## Objectifs, organisation, attendus


<!-- page_number: true -->
<!-- footer: Pôle enseignement numérique INSPE - UGA - 2023 - 2024 - CC:BY-NC-SA -->

---
# Objectifs EC 33
Cette UE a pour objectif de vous accompagner dans l'acquisition des compétences nécessaires pour utiliser le numérique en classe en prenant en compte les dimensions, efficacité de l’enseignement, respect des normes juridiques et éthiques et développement de la culture numérique des élèves. 

Cette formation articule apports théoriques et activités pratiques aboutissant à la conception puis la mise en œuvre et enfin l’analyse réflexive d’une ou plusieurs situations d’apprentissage intégrant le numérique.


---
# Organisation

* 3 CM 
* 5 TD 
:warning: Vous aurez différents intervenants 

---
# Attendus EC33 (partie évaluée pour le Master)
- Un document rédigé **individuellement** de 4 pages maximum comportant un descriptif de la situation d'apprentissage avec obligatoirement des liens pointants sur les éléments de conception (rappel la conception de la situation d'apprentissage peut être réalisée en groupe), cette partie doit impérativement contenir une argumentation sur les choix opérés lors de la conception, notamment au regard des apports théoriques, seule cette partie sera prise en compte dans le cadre de l'évaluation de l'EC 33; 
- Nouveauté 2023 (peut-être):arrow_right: Une trace attestant de l'entrée dans le volet automatisé de PIX+EDU 

---
# Objectifs PIX+EDU
:warning: **À ce jour il n'y a pas de texte officiel, donc on ne le fait pas à Grenoble... pour l'instant**
Le travail demandé pourra s’articuler avec un ou des travaux réalisés dans d’autres UE ou autour du mémoire et surtout il devra permettre de développer au maximum le travail attendu pour répondre aux exigences de la certification PIX+EDU (nouveauté 2023)

Le dispositif PIX+EDU comporte deux volets :
* un volet automatisé qui doit être initié à l’INSPE et qui sera finalisé en établissement avant le premier rendez-vous de carrière.
* un volet pratiques professionnelles réalisé entièrement à l’INSPE.

---
# Attendus pour la certification PIX+EDU
> - Attendus pour l'EC33
## +
> - mise en oeuvre de la situation d'appentissage face à des apprenants en classe ou en modalité simulée;
> - rédaction d'un compte rendu avec obligatoirement des liens pointant sur des traces de cette mise en oeuvre.
> - rédaction d'une analyse réflexive sur cette mise en oeuvre, cette partie doit faire émerger des pistes d'amélioration.

---
# :sos: Ressources
#

#### Cours sur la plateforme eformation :arrow_right:  https://eformation.univ-grenoble-alpes.fr








