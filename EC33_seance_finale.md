---
marp: true
theme: aqua
autoscale: true

---

![w:400](images/img-effets/Effets-logo-nouvelle-uga.png)

# EC-33 séance finale M2-PE


---
# Rappel des attendus de la production finale
## Format général
* Un document par binôme
* Un seul document au format PDF déposé sur la plateforme, ce document peut de façon optionnelle contenir des liens pointant vers des documents ou ressources externes (Attention, l’accessibilité à ces documents doit être garantie).
* Taille maximum 6 à 7 pages
* Police Arial, taille 12, interligne 1,5 cm, marges 2,5 cm

 

---

# Contenu de la production (1/3)

## Description de la situation d’apprentissage choisie, une situation par binôme (1 page)
:warning: Important : Ce n’est pas la qualité de la situation qui sera jugée mais l’analyse qui en sera faite.

La description sera rédigée en veillant à : 
* ce qu’elle soit compréhensible par d’autres étudiants, 
* fournir toutes les informations nécessaires permettant à d’autres de s’approprier cette situation afin de la mettre en œuvre avec leurs élèves.

---
# Contenu de la production (2/3)


## Analyse de la situation d’apprentissage choisie à la lumière de chacun des thèmes abordés durant la formation (4 à 5 pages max)
A la fin de chaque séance thématique, le binôme a rédigé une courte analyse la situation d’apprentissage au regard des éléments, théoriques ou pratiques, abordés dans la séance. Les cinq analyses sont à mettre dans des paragraphes séparés.


---
# Contenu de la production (3/3)


## Bilan réflexif (1 à 2 pages max)

Ce bilan réflexif doit permettre d'évaluer les compétences suivantes :
* Être capable de mettre en relation les éléments d’amélioration de la situation de départ issus des différentes thématiques, et d’en montrer la cohérence ; 
* Être capable d’avoir un jugement critique sur l’utilisation du numérique en situation scolaire, notamment en en repérant les avantages et inconvénients ; 
* Être capable de transférer l’analyse de la situation de départ au profit de nouvelles situations (dans autres usages, outils, disciplines, ou niveaux).

---
# Délais 

## :warning: La production sera déposée sur la plateforme pour évaluation au plus tard 48 h après la dernière séance en présence.

Conseil : déposez votre production en fin de TD.

---
# Plagiat

Tout plagiat détecté sera signalé aux instances universitaires compétentes.

---
# Information PIX+EDU
* Nouvelle certification (en phase expérimentale, généralisation prévue en septembre 2023)
* Objectif : certifier les compétences "métiers" des professeurs 
* Certification en deux volets
* Obligation à partir de la rentrée 2023 ? :arrow_right: Pas de texte officiel à ce jour
* Référentiel et critères d'évaluation non validés



---

![](images/img-effets/pix_edu_p1.png)

---

![](images/img-effets/pix_edu_p2.png)

---
# Nos conseils 
* Inscrivez-vous et réalisez les tests de positionnement sur PIX (voir l'item Tests de positionnement sur le cours moodle).
* Conservez précieusement le dossier que vous avez constitué pour l'UE 33 (et prenez en compte les retours formatifs)
* Essayez de réaliser une mise en oeuvre d'un usage du numérique

Vous êtes volontaire pour la phase expérimentale de la certification PIX+Edu :arrow_right: inscrivez-vous sur le cours moodle item  **"Inscription expérimentation PIX+EDU"**


