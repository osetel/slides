---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Culture numérique et Apprentissage

## UE - Num 801 - Séance 7  
Equipe formateurs numérique - UGA - 2023

---
<!-- page_number: true -->
<!-- footer: 2023 - CC:BY-NC-SA -->
# Analyse réflexive sur les usages du numérique en situation de classe

Cette situation sera analysée en vous aidant des apports délivrés au cours des 5 séances thématiques :

* Production et usages de ressources numériques
* Evaluer et s’auto-évaluer avec le numérique
* Effets du numérique sur les apprentissages
* Numérique et juridique
* Enseigner avec le numérique : quelques zones à risque

Chaque analyse fera entre 1/2 page à une page.

---
## Attendus sur la thématique "Production et usages de ressources numériques"

- Recenser les principales ressources 
- Expliciter la production de ces ressources
- Expliciter leurs mises en œuvre en insistant sur le mode de diffusion
- Analyser les objectifs et les plus-values

---
# Attendus sur la thématique "Evaluer et s’auto-évaluer avec le numérique"

- Identifier dans la situation où le numérique est (ou pourrait être) utilisé pour l’évaluation. 
- Envisager et présenter plusieurs usages et outils du numérique pour l’évaluation
- Analyser leurs plus-values et limites.

---
# Attendus sur la thématique "Effets du numérique sur les apprentissages"

- A partir de votre situation d'apprentissage, essayez d'évaluer les effets du numérique sur les apprentissages, sont-ils positifs ou risquent-ils d'êtres négatifs ?
- Proposez une ou des évolutions destinées à limiter les effets négatifs et produire des effets positifs.
    
---
# Attendus sur la thématique "Numérique et juridique"

- Identifier les problèmes juridiques potentiels dans la situation
- Donner les précautions prises ou à prendre.
- Proposer une action concrète permettant d'éduquer les élèves à ces problèmes

Vous devez donner la preuve de ce que vous avancez en mettant des liens vers les conditions générales d'utilisation des applications (ou ressources) utilisés et/ou leur politique de confidentialité.

---
# Attendus sur la thématique "Enseigner avec le numérique : quelques zones à risque"

- À partir de votre situation d’apprentissage, essayez d’identifier une ou des zones à risque auxquelles vos élèves ou vous-même risquez d’être exposé et décrivez-en une en particulier.
- Si vous ne trouvez pas de zone à risques potentielle dans votre situation d’apprentissage, vous pouvez élargir à d’autres situations d’enseignement en prenant appui sur l’un des questionnements proposés après chaque zone à risque.
- Proposez des pistes d’actions pour tenter d’éviter ou de minimiser l’exposition à ces risques.

---
# Attendus Bilan réflexif


- Lister les avantages et inconvénients de la situation.
- Synthétiser les différents éléments d’amélioration de la situation de départ en mettant en avant les plus efficaces.
- Transférer l'analyse de la situation au profit de nouvelles situations (dans d'autres usages, outils, disciplines, ou niveaux).
