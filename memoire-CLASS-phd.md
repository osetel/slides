---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Atelier Mémoire “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes
##### Année universitaire 2022-23

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1-M2 PE • Atelier mémoire “Interactions enseignant-élèves • Inspé-UGA 2022-23 • CC:BY-NC-SA-->
<!-- paginate: true -->

# Séance 1 M1 PE - 14/12/22

- Présentation de chacun.e
- Rappel des dimensions du CLASS
- Discussion sur le contexte de classe

---
# 1 M1 - Présentation rapide
- prénom ; niveau de classe de stage ; caractéristiques principales de l'école, de la classe (si connues)

---
# 1 M1 - Syllabus des séances de mémoire
- Site plate-forme :  [https://eformation.univ-grenoble-alpes.fr/group/index.php?id=332&group=80](https://eformation.univ-grenoble-alpes.fr/group/index.php?id=332&group=80) ;  mot de passe pour l’auto-inscription : **MemoirePhD**
- Syllabus : https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/syl_CLASS_espe.html
- Vidéo sur la problématique : https://videos.univ-grenoble-alpes.fr/video/25619-formuler-un-probleme-de-recherche-memoire-meef-interactions-enseignants-eleve/



---
# 1 M1 - Préambule (1/2) : Règles de fonctionnement du groupe

- Une partie du travail de l’atelier est fondée sur la confrontation entre participants de leurs pratiques de classe, leur compréhension et leur amélioration. Il s’agit de les rapporter le plus précisément possible et, en contre-partie, d’éviter de porter des jugements sur la personne qui a rapporté les pratiques, mais d’évaluer le plus objectivement possible ce qui s’est passé (et non la personne qui est à l’origine possible de ces pratiques).
- Ce travail d’analyse des pratiques peut être outillé par le CLASS (un outil d’observation de la qualité des interactions enseignant-élèves). Le formateur et les participants mettront tout en œuvre pour permettre une parole libre, positive et constructive à propos des pratiques de chacun.

---
# 1 M1 - Préambule (2/2)

- Le résultat final de l’atelier mémoire fera l’objet d’une évaluation, et non les pratiques relatées ou visionnées au cours de l’atelier.
- Les propos tenus au sein du groupe reposent sur la **confidentialité **(c.-à-d. que les participants ne peuvent rapporter ce qui s’est dit dans l’atelier à des personnes n’en faisant pas partie) et le respect de la parole de chacun des membres.
- Le CLASS n'est pas un outil de l'évaluation de l'enseignant·e, mais bien de ce qui se joue entre l'enseignant et ses élèves.

---
# 1 M1 - Organisation sommaire de l'année (1/2)

- en Master 1, le travail lié au mémoire est à réaliser en binôme ;
- le contexte théorique, la méthode de l’étude et le recueil de données seront réalisés dans l’année de Master 1 ;
- la partie du mémoire rendue en fin de Master 1 comportera donc : 
	– la partie théorique, 
	– la problématique, 
	– la méthode ; 
	– le recueil de données ;

---
# 1 M1 - Organisation sommaire de l'année (2/2)

- les données recueillies le seront **exclusivement** pendant le stage filé (de janvier à mai 2022) ;
- la partie “méthode” ne sera pas évaluée en Master 1 ; il est toutefois vivement conseillé d’en écrire une version la plus aboutie possible, nécessaire pour le recueil de données et pour comprendre la démarche ;
- le mémoire sera fini durant le premier semestre de Master 2 : écriture de l’analyse des données, de la discussion/conclusion ; peaufinage des parties livrées en Master 1.

---
# 1 M1 - Le rôle de CLASS
- CLASS est un outil d'observation de la qualité des interactions enseignant·e/élèves
- Nous allons l'utiliser plutôt, dans ces séances de tutorat, comme un outil
	- permettant d'avoir un vocabulaire commun
	- permettant de vous centrer sur des pistes d'analyse de situations et d'amélioration
	
---
# 1 M1 - Revue rapide du système CLASS
Pour s'assurer d'une bonne compréhension

---
# 1 M1 - Domaine 1 – Soutien émotionnel
- Des relations chaleureuses, soutenantes avec les adultes et les autres enfants
- La joie et le désir d'apprendre
- Une motivation à s'engager dans les activités d'apprentissage
- Un sentiment de confort (aisance) dans le milieu éducatif
- La volonté de relever des défis sur le plan social et académique
- Un niveau approprié d'autonomie

---
# 1 M1 - Soutien émotionnel - 1 Climat positif (C+)
Reflète le lien émotionnel entre l'enseignant et l'enfant, entre les enfants eux-mêmes ainsi que la chaleur, le respect et le plaisir communiqué dans les interactions verbales et non-verbales

**Indicateurs**
- Relations
- Affect positif
- Communication positive 
- Respect

---
# 1 M1 - Soutien émotionnel - 2 Climat négatif (C–)
N'est pas simplement l'absence de climat positif mais bien la présence de comportements négatifs. Reflète l'ensemble de la négativité exprimée dans la classe. La fréquence, l'intensité et la nature de la négativité de l'enseignant et des enfants sont des éléments-clés de cette dimension

**Indicateurs**
- Affect négatif
- Contrôle punitif
- Sarcasme/Irrespect
- Sévère négativité

---
# 1 M1 - Soutien émotionnel - 3 Sensibilité de l'enseignant (SE)
Englobe la capacité de l'enseignant à être attentif·ve et à répondre aux besoins émotionnels et académiques des enfants. Un haut niveau de sensibilité offre un soutien aux enfants dans l'exploration et l'apprentissage puisque l'enseignant leur offre de manière constante du réconfort et de l'encouragement

**Indicateurs**
- Conscience/vigilance
- Réceptivité
- Réponse aux problèmes
- Confort des élèves

---
#  1 M1 - Soutien émotionnel - 4 Prise en considération du point de vue de l'enfant (PVE)
Permet de rendre compte dans quelle mesure les interactions de l'enseignant avec les enfants et les activités offertes mettent l'accent sur les intérêts des enfants, leur motivation et leur point de vue tout en encourageant la responsabilité de l'enfant et son autonomie

**Indicateurs**
- Souplesse et attention centrée sur l'élève
- Soutien à l'autonomie et au leadership
- Expression des élèves
- Restriction de mouvement

---
# 1 M1 - Domaine 2 – Organisation de la classe
Comment l'enseignant organise et gère différents aspects de la classe (comportement des élèves, temps, attention). Il y a une meilleure opportunité d'apprentissage et la classe fonctionne mieux si les élèves ont un comportement adéquat, cohérent avec les tâches qui leur sont allouées, et sont intéressés et engagés dans ces dernières.

---
# 1 M1 - Organisation classe - 5 Gestion des comportements (GC)
Englobe l'habileté de l'enseignant à établir des attentes comportementales claires et à utiliser des méthodes efficaces pour prévenir et rediriger les comportements inappropriés

**Indicateurs**
- Attentes comportementales claires
- Proactivité
- Redirection des comportements
- Comportements des enfants

---
# 1 M1 - Organisation classe  – 6 Productivité (P)

Dans quelle mesure l'enseignant gère le déroulement des activités et des routines de manière à optimiser le temps disponible à l'apprentissage ?

**Indicateurs**
- Maximisation du temps d'apprentissage
- Routines
- Transitions
- Préparation du matériel nécessaire à la séance

---
# 1 M1 - Organisation classe - 7 Modalités d'apprentissage (MA)

Reflète la manière dont l'enseignant maximise l'intérêt des enfnats, leur engagement et leur capacité à apprendre au cours des leçons et activités offertes

**Indicateurs**
- Accompagnement efficace
- Diversité des modalités et des matériels
- Intérêt de l'enfant
- Clarté des objets d'apprentissage

---
# 1 M1 - Domaine 3 – Soutien à l'apprentissage

S'intéresse à décrire comment les enseignants aident les enfants à 
- apprendre à résoudre des problèmes, à raisonner et à penser
- utiliser les rétroactions pour approfondir des habiletés et des connaissances
- développer des habiletés langagières plus élaborées

---
# 1 M1 - Soutien appr — 8. Développement de concepts (DC)

Utilisation par l'enseignant de stratégies diverses (discussions, activités) afin d'amener l'enfant à développer une pensée plus élaborée, une plus grande compréhension des phénomènes de son environnement plutôt que de favoriser un apprentissage par cœur

**Indicateurs**

- Analyse et raisonnement
- Créativité
- Intégration d'éléments déjà rencontrés dans les cours précédents
- Lien avec la vie réelle

---
# 1 M1 - Soutien appr — 9. Qualité de la rétroaction (QR)

Le niveau selon lequel l'enseignant offre des rétroactions aux enfants qui optimisent l'apprentissage et qui favorisent une participation maintenue

**Indicateurs**
- Étayage
- Rétroactions en boucle
- Stimuler les processus de la pensée
- Fournir de l'information
- Encouragement et affirmation
  
---
# 1 M1 - Soutien appr — 10. Modelage langagier (ML)

Rend compte de l'efficacité et de la quantité des techniques de stimulation du langage utilisées par l'enseignant.

**Indicateurs**
- Conversations fréquentes
- Questions ouvertes
- Répétition et extension
- *Self-talk* et *parallel-talk*
- Niveau de langage élaboré


---
# 1 M1 - Tour de table (1/2)
- Quel est le domaine (ou des dimensions dans un domaine) pour lequel vous vous sentez le/la plus à l'aise ?
- Quel est le domaine (ou des dimensions dans un domaine) pour lequel vous pensez avoir des difficultés ?
- :warning: Ces 2 impressions sont autant causées par vous que par vos élèves
- Considérez chaque dimension et annotez les possibles points forts/faibles

---
# 1 M1 - Liste des dimensions
1. Climat positif (C+)
2. Climat négatif (C–)
3. Sensibilité de l'enseignant (SE)
4. Prise en considération du point de vue de l'enfant (PVE)
5. Gestion des comportements (GC)
6. Productivité (P)
7. Modalités d'apprentissage (MA)
8. Développement de concepts (DC)
9. Qualité de la rétroaction (QR)
10. Modelage langagier (ML)

---
# 1 M1 - Pour commencer : la description du contexte !

La description du contexte de vos écoles (env. 1 page) figurera au début du mémoire

- niveau, nombre d’élèves, matières enseignées
- taille de l’école, autres spécificités, notamment matérielles et humaines (ATSEM),
- PCS (professions et catégorie socioprofessionnelles) majoritaires des parents (trouvable dans cette [carte](https://www.data.gouv.fr/fr/reuses/cartographie-de-lindice-de-position-sociale-des-ecoles-et-colleges-france-metropolitaine-et-drom/)),
- difficultés particulières de la classe, des élèves
- toute autre information permettant de mieux comprendre le contexte (matériel spécifique, emplacement de la classe, etc.)
- :warning: **attention à préserver l’anonymat de l’école et de la classe**

---
# 1 M1 - Travail à faire pour la prochaine séance

- Visionner la [vidéo](https://videos.univ-grenoble-alpes.fr/video/25619-formuler-un-probleme-de-recherche-memoire-meef-interactions-enseignants-eleve/) présentant une méthode pour déterminer une problématique, une ou des question.s de recherche, et des hypothèses.
- La lecture de ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-problematique.html) sur la problématique pourra aider
- Rédiger la présentation de votre contexte de classe (voir diapo précédente).
- Envoyer ces 2 documents au formateur pour commentaires avant le **20 janvier 2023**.


---
# 2 M1 - Séance du 25/01/23

Ordre du jour :
- point sur le contexte de classe
- point sur la problématique, les questions de recherche, les hypothèses
- point sur l'organisation de la suite
  
---
# 2 M1 - Récapitulatif - 1/2

 **Problème** : Issu de votre description du contexte. Par exemple : Certains élèves de PS ne parviennent pas à prendre la parole en groupe
- **Problématique** : Décrit mieux le problème et l'une de ses possibles solutions. Par exemple : Les rétroactions langagières de l'enseignant permettraient d'aider à la prise de parole des petit-parleurs de PS.
- **Question de recherche** : Reformule la problématique en C=F(S,E). Quelles rétroactions langagières de l'enseignant (E) pourraient rendre des élèves petit-parleurs de PS (S) plus participatifs (C) dans des séances de langage ?

---
# 2 M1 - Récapitulatif - 2/2 

- **Hypothèses** : Question de recherche dérivée en plusieurs sous-questions, formulées en prédictions plus précises et observables : 
	- **H1** - Les rétroactions langagières de l'enseignant vont permettre aux élèves petit-parleurs de s'exprimer plus souvent ; 
	- **H2** - Les rétroactions langagières de l'enseignant vont permettre aux élèves petit-parleurs de s'exprimer avec un meilleur niveau de langage.
- Possibilité de faire intervenir une autre variable qui pourrait avoir une influence dans le processus étudié. Par exemple : avoir plusieurs interventions différentes au lieu d'une seule ; ou une variable liée aux élèves : p. ex., leur niveau d'engagement dans les séances.

---
# 2 M1 - Agenda prévisionnel



	    ┌────────┐                     ┌───────────┐   ┌ ─ ─ ─ ─ ─ ┐
	    │Pré-test│                     │Post-test 1│    Post-test 2
	    └────────┘                     └───────────┘   └ ─ ─ ─ ─ ─ ┘
	        ■                                ■               ■
	        │                                │
	        │      ┌───────────────────┐     │               │
	        │      │ Interventions ≥ 3 │     │
	        │      └───────────────────┘     │               │
	        │                                │
	        │                                │               │
	        │                                │
	────────┴────────────────────────────────┴───────────────┴─────────▶

	  Début mars                     Début avril        Fin avril


---
# 2. - M1 Travail pour la prochaine séance

:warning: dates des prochaines séances à déterminer

- Finaliser la problématique, la ou les question(s) de recherche, les hypothèses ( -> envoi au formateur)
- Concevoir un premier jet de l'intervention (-> envoi au formateur)
- Commencer la revue de la question (après envoi d'un plan général au formateur courant février)

---
# 3. M1 - Séance du 8/02/23
##  Travail pour la prochaine séance (le 22/02/23 16:00-18:00)

:heavy_check_mark: Contexte 
:heavy_check_mark: Problématique, question de recherche, hypothèses
:construction: Évaluation du "niveau de base" dans votre classe
:construction: Finalisation de l'intervention, pour la passer dans les semaines suivantes (pendant env. 3 semaines)
⏭️ Plan de l'état de l'art
⏭️ État de l'art
  
---

# 4. M1 - Séance du 22/02/23
##  Travail pour la prochaine séance (le 15/03/23 13:45-15:45)

:heavy_check_mark: Contexte 
:heavy_check_mark: Problématique, question de recherche, hypothèses
:heavy_check_mark: Évaluation du "niveau de base" dans votre classe
:heavy_check_mark: Finalisation de l'intervention, pour la passer avant les vacances de printemps (pendant env. 3 semaines)
:construction: Plan de l'état de l'art
:construction: État de l'art

---
# 5. M1 - Séance du 15/03/23
##  Travail pour la prochaine séance (le 5/04/23 10:15-12:15)

:heavy_check_mark: Contexte 
:heavy_check_mark: Problématique, question de recherche, hypothèses
:heavy_check_mark: Évaluation du "niveau de base" dans votre classe
:heavy_check_mark: Finalisation de l'intervention, pour la passer avant les vacances de printemps (pendant env. 3 semaines)
:heavy_check_mark: Plan de l'état de l'art
:heavy_check_mark: État de l'art
:heavy_check_mark: Expérimentation


---
# 6 M1 - Séance du 5/04/23 Travail pour finaliser le mémoire

- :warning: La séance du 19/04 sera remplacée par une visio-conférence ou un rendez-vous en présence, par binômes (à solliciter au formateur)
- Les sections à livrer : la rédaction de la partie théorique, de la problématique et de la partie méthode du mémoire (sans les résultats et la discussion)
- **La version quasi-finale** du travail est à livrer pour le **26 mai**, pour relecture complète par le formateur
- Date de livraison pour évaluation dans le cadre du M1 (une note de CC sera donnée par binôme, avec des commentaires) : **le 12 mai 2023**
- :warning: Il est vivement conseillé de faire votre possible pour terminer la rédaction complète de votre mémoire d'ici à début septembre 2023


<!--
# :warning: **Attention, les diapositives qui suivent n'ont pas été mises à jour**

--- 
# M1&M2 - Séance du 16 février 2022

- M1 : avez-vous des questions sur la vidéo ?

---
# M1&M2 Récapitulatif (1/2)
- **Problème** : Issu de votre description du contexte. Par exemple : Certains élèves de PS ne parviennent pas à prendre la parole en groupe
- **Problématique** : Décrit mieux le problème et l'une de ses possibles solutions. Par exemple : Les rétroactions langagières de l'enseignant permettraient d'aider à la prise de parole des petit-parleurs de PS.
- **Question de recherche** : Reformule la problématique en C=F(S,E). Quelles rétroactions langagières de l'enseignant (E) pourraient rendre des élèves petit-parleurs de PS (S) plus participatifs (C) dans des séances de langage ?


---
# Séance M2 PE : Point sur la soutenance (9/12/22)

---
# M2 Séance collective 

Buts : 
- point sur la soutenance orale

Site Moodle du mémoire :  https://frama.link/UE406
[Site Inspé](https://inspe.univ-grenoble-alpes.fr/formations/professorat-des-ecoles-/le-memoire-master-meef-pe-252524.kjsp?RH=1498479210386)

---
# Dépôt du mémoire

- la date-butoir de dépôt : **6 janvier 2023**
- soutenances entre le **16 et 26 janvier 2023**
- l’envoi par le dispositif demandé (Moodle) est le seul légal. Le formateur enverra les liens des mémoires aux jurés.
- doubler par un envoi du mémoire par courriel à l'encadrant
- pas de version sur papier nécessaire pour le jury

---
# Mémoire : Finalisation de l’écriture (1/3)

- S’assurer qu’aucun élève/parent ne puisse être reconnu, même anonymé. Attention aux jugements que vous portez
- S’assurer que le mémoire correspond aux documents de cadrage (Présentation générale et Guide de rédaction)
- Présentez vos résultats en lien avec vos hypothèses, et seulement elles. Essayez de trouver une explication pour celle(s) non validée(s)
- N'oubliez pas que les 1re et 4e de couverture ont des modèles à respecter (voir site)

---
# Mémoire : Finalisation de l’écriture (2/3)

- Référer, notamment en discussion, au programmes et au référentiel de compétences des enseignants en discussion (dire la/les comp. favorisées par le mémoire)
- Prendre notamment soin de vérifier que la page de couverture contient les logos nécessaires, et que la 4e de couv. contient le résumé et les mots-clés
- Le résumé en anglais peut être produit (et vérifié ensuite) par [deepl](https://www.deepl.com/translator), le meilleur traducteur gratuit

---
# Mémoire : Finalisation de l’écriture (3/3)

- Le formulaire de non-plagiat est à remplir lors du dépôt du mémoire 
- Déposer séparément l'accord de dépôt du mémoire/ESR sur Dumas https://cloud.univ-grenoble-alpes.fr/index.php/s/goLQ4Tws944DqXr
- voir grille d'évaluation dans le site


---
# Mémoire : Principaux écueils (écrit) (1/2)

- Parties plagiées (:warning: tous les mémoires seront vérifiés avec Compilatio et leurs rapports envoyés pour vérification)
- Maîtrise insuffisante de la langue française (faire relire le mémoire par des tiers)
- Pas de problématique claire
- Pas de liens (ou liens insuffisants) entre partie théorique et empirique 

---
# Mémoire : Principaux écueils (écrit) (2/2)

- Partie empirique peu claire (on ne comprend pas ce que vous avez réalisé vous-même)
- Partie empirique non analysée
- Analyse peu fouillée des données recueillies
- Analyse réflexive faible, manque de recul
- Texte très court, mal documenté ; texte trop long, explications embrouillées
- Pas de respect de la vie privée et/ou du droit à l’image (photos où l'on peut reconnaître des personnes, prénoms d'élèves non anonymés)

---
# Soutenance : Rappel des conditions (voir doc. Cadrage mémoire) / 1. Présentation

La soutenance (20 min) est individuelle (même quand ESR faits entièrement à 2, DU). Elle comporte deux parties :

- Une présentation (**10 min**) focalisée sur des aspects choisis du mémoire. Elle met en évidence le cheminement professionnel et souligne les aspects les plus formateurs du travail. La présentation orale s’appuie sur des supports de communication.

---
# Soutenance : Rappel des conditions (voir doc. Cadrage mémoire) / 2. Discussion (1/2)

- Une discussion avec le jury (**10 min**), au cours de laquelle l’étudiant témoigne de son appropriation de la problématique, de sa maîtrise du sujet et d’une prise de recul.
- La soutenance étant publique, vous pouvez inviter des personnes (étudiants de l’atelier, tuteur/trices, parents, amis, etc.) à venir écouter (ils n'interviendront pas)

---
# Soutenance : Rappel des conditions (voir doc. Cadrage mémoire) / 2. Discussion (2/2)

- Quand ils ne font pas partie du jury, les professionnels de terrain sont invités à la soutenance mais ils ne participent pas aux délibérations. 
- Note d'écrit compte pour 65 % de la note finale, la note d'oral compte pour 35 %

---
# Conseils pour la soutenance (1/2)

- Avoir lu la grille d'évaluation du mémoire, vérifier que votre travail répond aux différents critères
- Respecter **scrupuleusement** le temps (le jury peut vous arrêter une fois le temps écoulé), donc, répéter
- Réaliser des diapositives **lisibles**, sans animation, et en nombre réduit (une dizaine max.). Ne pas copier-coller de tableaux ou images qui seront illisibles
- (Si soutenance en présence, venir avec une clé USB et la présentation en PDF (pour gagner du temps) : l'ordinateur est fourni)

---
# Conseils pour la soutenance (2/2)

- Le jury a lu le mémoire, donc lui rappeler les éléments essentiels (d’un point de vue théorique et empirique) ; insister sur les aspects liés au métier d’enseignant, ce qui a été appris en faisant ce mémoire
- Le jury ne cherche pas à vous tendre des pièges : il veut plutôt comprendre votre démarche, ce que vous avez fait et pourquoi. Répondez clairement et le plus **succinctement** possible à ses questions

---
# Écueils pendant la soutenance

- Lire les diapositives sans ajouter de plus-value (effet karaoké)
- Présenter des copies d’écran du mémoire illisibles (“vous ne pouvez pas lire cette diapo, mais...”)
- Présenter des diapositives trop chargées (“ce qui est important ici est ceci...”)
- Se perdre dans des anecdotes, ou dans la théorie

---
# Grille d’évaluation (résumé *cf.* la grille complète)

- Ecrit
	- Se former et innover par la rédaction d’un mémoire professionnel
	- Maîtriser la langue française
- Oral
	- Exposer d’une manière critique l’analyse d’une pratique pro.
	- Communiquer efficacement à l’oral
- Note modulée en fonction de l’implication de l’étudiant (avis de l’encadrant)

---
# Valorisation

- Les meilleurs mémoires feront l’objet d’une présentation orale lors de la Journée de l’école organisée en fin d’année, le **date à venir** à l'Inspé de Chambéry sur proposition de leurs encadrants

---
# Après le mémoire

- Les mémoires ayant eu une note d’au moins **16/20** sont archivés publiquement (base DUMAS), après accord de leurs auteurs
- :warning: vous pouvez être démarchés par des éditeurs à compte d’auteur. N’accepter l’édition qu’après avoir lu de près le contrat (notamment en ce qui concerne l’achat d’exemplaires et la cession des droits). Sachez que ce type d'édition ne compte pas ou peu pour un éventuel projet de carrière universitaire.

---
# Des questions ? 

- N'hésitez pas à les poser par courriel à votre formateur
-->