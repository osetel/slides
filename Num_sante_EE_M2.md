---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  

# Numérique et santé MEEF EE
 
### Christophe Charroud - Univ. Grenoble Alpes


<!-- page_number: true -->
<!-- footer:  christophe.charroud@univ-grenoble-alpes.fr – M2 MEEF EE – 2023 - CC:BY-NC-SA  -->

---
# Au programme

1. Introduction et composition des équipes
2. Etude d'un sujet par équipe (1h15)
3. Présentation collective des résultats


---

# :one: Introduction et composition des équipes
Durée 5 minutes

---
## Numérique et santé, pourquoi cette question ?
* Problèmes de santé publique
* Informations véhiculées par les médias
* Que dit la recherche ?
* :arrow_right: Rôle du CPE


---
## Constitution de 3 équipes
Chaque équipe va réaliser une présentation sur un sujet parmi la liste ci-dessous :
* Écrans, sommeil et problèmes ophtalmiques, quels liens, quels effets ?
* Numérique et impacts neurologiques (trouble du développement,TSA, TDAH, ...) quels liens, quels effets ?
* Numérique et impacts sur l'hygiène de vie (sédentarité, ondes WIFI, 4g, 5g , TMS...) quels liens, quels effets ?
* *ou autre sujet présenté par une équipe et validé par l'enseignant...*


---
# :two: :pencil: Recherche documentaire et conception des présentations
:warning: Fin de l'activité à 14h50 

---
## Consignes 

1 - Chaque équipe recherche des documents et des arguments **scientifquement étayés** sur son sujet (références obligatoires).
2 - Chaque équipe prépare une présentation d'une durée de 8 à 10 minutes avec l'appui d'un support visuel.


---

# :three: Présentations 

---

# :four: Débat - Conclusion

---
# :five: Travail sur la production attendue de l'UE
*Durée 1h30*
## Rappel :
En binôme, les étudiants devront produire un document décrivant une formation/information/sensibilastion sur un thème lié aux usages du numérique. Lors de chaque séance, un temps sera reservé à la construction de cette production. Cette production sera élaborée tout au long de la formation et elle peut bénéficier des retours formatifs donnés par tous les intervenants. Il sera à rendre.

