# Formation au Numérique de l'Inspé de l'univ. Grenoble Alpes
# Site des présentations
# Année 2023-24

## Formations MEEF

## EC 33

-  Séance d'intro M2 PE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/intro_pe_23_24.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/intro_pe_23_24.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/intro_pe_23_24.pdf)]

- Effets 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Effets_23_24.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Effets_23_24.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Effets_23_24.pdf)]

-  Effets PE Annecy séance de 4h avec TD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Effets_annecy.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Effets_annecy.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Effets_annecy.pdf)]

- TD en lien avec le CTD Effets
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Effets_TD.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Effets_TD.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Effets_TD.pdf)]

- Ressources et droit d'auteur
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/EC33-RessourcesDroits.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/EC33-RessourcesDroits.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/EC33-RessourcesDroits.pdf)]

- Infox-IA
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/infox-ia.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/infox-ia.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/infox-ia.pdf)]

## DIU PE
-  Séance d'intro DIU PE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/intro_DIU_PE_23_24.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/intro_DIU_PE_23_24.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/intro_DIU_PE_23_24.pdf)]



## DIU SD
-  Séance d'intro DIU SD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/intro_DIU_SD_23_24.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/intro_DIU_SD_23_24.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/intro_DIU_SD_23_24.pdf)]

- Effets 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Effets-DIU-SD.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Effets-DIU-SD.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Effets-DIU-SD.pdf)]


## Formation MEEF-PIF

- UE 301 Évaluer l'apprentissage par ordinateur : Tâches et rétroactions 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/UE_301_MEEF-PIF.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE_301_MEEF-PIF.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE_301_MEEF-PIF.pdf)]

# __________________ Archives__________________
## Année 2022-23

### Formation MEEF

- UE-Evaluer
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Ue_404_Evaluer.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Ue_404_Evaluer.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Ue_404_Evaluer.pdf)]

#### EC 33
- UE-Zones à risques [[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/UE_404_Zone_risques.md)][[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE_404_Zone_risques.html)][[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE_404_Zone_risques.pdf)][[Lien raccourci : https://link.infini.fr/404zar](https://link.infini.fr/404zar)]

- UE Enseigner avec le numérique : infox, IA, & éthique [[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/infox-ia.md)][[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/infox-ia.html)][[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/infox-ia.pdf)]


- Juridique M2 et DIU PE 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/juridiquePE.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/juridiquePE.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/juridiquePE.pdf)]

- Effets du numérique sur les apprentissages - EC33 PE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/EC33_Effets.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/EC33_Effets.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/EC33_Effets.pdf)]

- Séance finale - EC33 PE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/EC33_seance_finale.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/EC33_seance_finale.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/EC33_seance_finale.pdf)]

#### UE801 SD
- Séance 1 - UE801 M1 SD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/UE801_seance1.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE801_seance1.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE801_seance1.pdf)]

- Effets du numérique sur les apprentissages - UE801 M1 SD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/UE801-Effets.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE801-Effets.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE801-Effets.pdf)]

- Évaluer et s'autoévaluer avec le numérique - UE801 M1 SD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/UE801_Evaluation.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE801_Evaluation.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE801_Evaluation.pdf)]

- Numérique et Juridique UE801 M1 SD et DIU SD 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/juridiqueSD.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/juridiqueSD.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/juridiqueSD.pdf)]

- Séance 6 - UE801 M1 SD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Seance6-M1-SD.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Seance6-M1-SD.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Seance6-M1-SD.pdf)]

- Séance 7 - UE801 M1 SD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Seance7-M1-SD.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Seance7-M1-SD.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Seance7-M1-SD.pdf)]


#### Autres formations

- UE14 - Problématisation - M1 PE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/UE14.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE14.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE14.pdf)]
[[Raccourci : https://link.infini.fr/ue-prob-uga](https://link.infini.fr/ue-prob-uga)]

- Séances tutorat : 
  - Philippe Dessus 
  [[Source S1](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/tutorat_pe_phd_S1.md)]
  [[HTML S1](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/tutorat_pe_phd_S1.html)] 
  [[PDF S1](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/tutorat_pe_phd_S1.pdf)]

- Présentations mémoire 
  - Philippe Dessus
  [[Source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/memoire-CLASS-phd.md)]
  [[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/memoire-CLASS-phd.html)]
  [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/memoire-CLASS-phd.pdf)]

### MEEF EE

- Séance 1 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Seance1-EE.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Seance1-EE.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Seance1-EE.pdf)]

- Numérique et santé M2 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Num_sante_EE_M2.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Num_sante_EE_M2.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Num_sante_EE_M2.pdf)]

- Juridique EE 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/JuridiqueEEcours.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/JuridiqueEEcours.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/JuridiqueEEcours.pdf)]

- Zones à Risque EE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/UE41-Zone-a-risque.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE41-Zone-a-risque.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE41-Zone-a-risque.pdf)]


## Formation MEEF DIU

- Diaporama introductif - Module DIU SD 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Diaporama-DIU-SD.md)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Diaporama-DIU-SD.pdf)]
[[HTM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Diaporama-DIU-SD.html)]

- Diaporama introductif - Module DIU EE 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Diaporama-DIU-EE.md)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Diaporama-DIU-EE.pdf)]
[[HTM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Diaporama-DIU-EE.html)]

- Diaporama introductif - Module DIU PE 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Diaporama-DIU-PE.md)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Diaporama-DIU-PE.pdf)]
[[HTM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Diaporama-DIU-PE.html)]

- Effets du numérique sur les apprentissages - Module DIU PE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Module_PE_Effets.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Module_PE_Effets.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Module_PE_Effets.pdf)]

- Effets du numérique sur les apprentissages - Module DIU SD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/ModuleSD-Effets.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/ModuleSD-Effets.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/ModuleSD-Effets.pdf)]

- Evaluer et s'autoévaluer avec le numérique - Module DIU SD
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/DIU-SD-Eval.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/DIU-SD-Eval.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/DIU-SD-Eval.pdf)]

- Zones à risques - Module DIU PE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/ZAR-module-PEmd)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/ZAR-module-PE.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/ZAR-module-PE.pdf)]

- Evaluer et s'autoévaluer avec le numérique - Module DIU PE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/ModulePE-eval.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/ModulePE-eval.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/ModulePE-eval.pdf)]

- Numérique et santé - Module DIU EE
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Numérique et santé - Module EE.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Numérique et santé - Module EE.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Numérique et santé - Module EE.pdf)]


- Éducation fondée sur les faits – Module DIU PE
[Source]()
[HTML]()
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/DIU-PE-Rech.pdf)]



## Formation Éducation à la santé

- Addictions 
[[source](https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/slides/-/blob/main/Sante_Addictions.md)]
[[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Sante_Addictions.html)]
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Sante_Addictions.pdf)]



[^1]: 
