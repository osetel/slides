---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Culture numérique et Apprentissage

## UE - Num 801 - Séance 6  
Equipe formateurs numérique - UGA - 2023

---
<!-- page_number: true -->
<!-- footer: 2023 - CC:BY-NC-SA -->

# Séance en deux phases :
### Phase 1 
* Évaluation  : durée 30 minutes 
(40 mn pour ceux bénificiant d'un tiers temps)

### Phase 2
#### Analyse réflexive sur les usages du numérique en situation de classe
* Constitution de binôme 
* Choix d'une situation d'apprentissage intégrant le numérique
* Rédaction de la description de la situation d'apprentissage

---
# Phase 1
## Évaluation CC1

Code d'accès au test :
## 

---
# Phase 2
## Analyse réflexive sur les usages du numérique en situation de classe

* Constitution de binôme 
* Choix d'une situation d'apprentissage intégrant le numérique
* Rédaction de la description de la situation d'apprentissage
 
 
---
# Pourquoi?
* 	Pour être capable de se poser des questions sur les plus-values supposées des situations d'apprentissage intégrant le numérique lors de leur conception.
* 	Pour réduire le plus possible les risques juridiques, éthiques, physiologiques et effets négatifs en termes d'apprentissages induits par les usages du numérique.
* 	Pour être capable d'analyser objectivement une situation d'apprentissage déjà réalisée et pouvoir en proposer une ou des évolutions bénéfiques pour les élèves.
# 
:arrow_right: PIX+EDU

---
# Comment?
* Via l'analyse en **binôme** d'une situation d'apprentissage, **vécue, observée ou rapportée** utilisant le numérique.
* Cette situation sera analysée en vous aidant des apports délivrés au cours de 5 séances thématiques :
	* Numérique et juridique
	* Effets du numérique sur les apprentissages
	* Enseigner avec le numérique : quelques zones à risque
	* Evaluer et s’auto-évaluer avec le numérique
	* Production et usages de ressources numériques

---
## Processus

![width:600px](images/ProcessusAnalyse.png)

---
# Qu'est-ce qui est évalué?
* Un document de 6 pages **MAXIMUM** réalisé en **binôme** contenant :
	* Une description de la situation d'apprentissage intégrant le numérique que vous avez choisie
	* Une analyse en lien avec chacune des thématiques abordées en formation
	* Un bilan réflexif 
	
## Ce n'est pas la qualité de la situation intiale qui sera jugée mais l'analyse qui en sera faite !

---
# Barème

- Chaque thématique (/3)
- Bilan réflexif (/5)

1 point pourra être retiré en cas de non-respect de la forme, du délai, des règles d’orthographe et de grammaire.

(Voir le détail dans le Syllabus)

---
# Précision
La description de la situation n'est pas évaluée directement, mais doit permettre d'apporter suffisamment d'informations pour éclairer les analyses dans chaque thèmatique.


---
# Constitution des binômes

* Soit par affinité
* Soit en fonction d'une idée commune
* ?

---

# Inscription des binômes sur le Moodle

2 étapes:

* L’un des étudiants crée un groupe en indiquant les deux noms dans le titre
* Le deuxième étudiant s’inscrit dans le groupe qui vient d’être ouvert

---
# 1. Création du groupe

Inscription du premier participant
![width:400px](images/seance6M1SD/GroupeE1M1SD.png)
![width:500px](images/seance6M1SD/GroupeE2.png)
![width:500px](images/seance6M1SD/GroupeE3PE.png)
Nom du groupe de la forme Chambery-GX ou Grenoble-GX

---

# 2. Inscription du 2e participant

Inscription du deuxième participant
![width:800px](images/seance6M1SD/GroupeE4PE.png)

Désinscription possible
![width:800px](images/seance6M1SD/GroupeE5PE.png)

---
## Rédaction de la description 

La description doit fournir les informations suivantes de manière synthétique :
```
* Discipline(s) concernée(s) par la situation
* Niveau de classe
* Objectif(s) d’apprentissages :
	- Compétences disciplinaires visées 
	- Compétences numériques développées auprès des élèves (issues Crcn)
* Environnement numérique impliqué dans la situation
	- Outils physiques (TBI, tablettes, ordinateurs,...)
	- Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
	- logiciels ou applications utilisés.
* Mise en œuvre
	- Organisation pédagogique (quelles activités, quand, comment, ...)
	- Production(s) attendue(s)
	- Modalités d'évaluation des compétences disciplinaires visées
```

---
# Prochaines séances

* La séance 7 est consacrée à un travail d’analyse mettant en application les apports des 5 thématiques à la situation.

* La séance 8 est consacrée à une mise en commun des différentes analyses, pour les mutualiser et les améliorer par la discussion.

### La production sera déposée sur la plateforme pour évaluation au plus tard le 25 mars 2023 à 23:59.

---
# Important : PIX+EDU
![](images/img-effets/logo_pix_edu_small.png)

Conservez tous vos travaux réalisés dans le cadre de cette UE Num 801, ils pourront éventuellement vous servir pour la certification PIX+EDU.





