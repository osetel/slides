---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
# 


# Évaluer et s'auto-évaluer avec le numérique

## Module numérique 1 et 2 - DIU PE
   
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr- UGA - Évaluer avec le numérique PE - 2023 - CC:BY-NC-SA -->

---
## À méditer avant de commencer...
“ […] les effets des mauvaises pratiques  sont bien plus puissants [dans le cas de l’évaluation] que dans tout autre aspect de l’enseignement.
Les élèves peuvent, avec difficulté, s’échapper des effets d’un mauvais enseignement ; ils ne peuvent (s’ils veulent réussir dans un cours) échapper aux effets d’une mauvaise évaluation. ” 

(McDonald et al., 2000)

---
# 

# :one: Évaluer et s'auto-évaluer (avec ou sans numérique).
## Ce que l'on sait.

---
## Évaluer - Une définition

L’évaluation en éducation est le processus par lequel **on délimite, obtient, et fournit des informations utiles permettant de juger des décisions possibles”**. ([Stufflebeam et al., 1980] p. 48)

:arrow_right: Évaluer ce n'est pas nécessairement donner une note


---
## Évaluer 

* C’est un processus (dynamique, se déroulant dans le temps et l’espace), se prépare (délimite), pour observer, et recueillir des informations qu’on synthétise pour les diffuser. 
* L’évaluation est orientée vers des buts précis (on n’évalue pas pour évaluer, mais pour agir, prendre des décisions à propos des apprenants).


---
## Types d'évaluation (1/2)

Les types d'évaluation varient (doivent varier) en fonction :
* du moment de l'apprentissage (au début, pendant, à la fin, plus tard...)
* du support (écrit, oral, physique, comportemental...)
* des processus cognitifs scrutés (savoir, réflexion, construction d'un raisonnement...)
* du contexte (individuellement, en groupe, en stage...)
* des décisions à prendre (positionnement, régulation, certification, orientation...)

---
## Types d'évaluation (2/2)

”Lorsque le cuisinier goûte la soupe, c'est formatif ; lorsque les invités goûtent la soupe, c'est sommatif” (R. Stake cité par Scriven 1991 p. 169)

- *évaluation POUR l'apprentissage* (formative), **évaluer en jugeant**, où la qualité de la production est *directement* évaluée par l'élève, aidé par l'enseignant

- *évaluation DE l'apprentissage* (sommative), **évaluer en testant**, où des inférences sont faites entre l'évaluation de réponse à un test et les compétences de l'élève (on goûte une soupe pour évaluer la qualité du cuisinier)

:warning: Il ne s’agit pas de dire que l’une de ces deux est meilleure que l’autre, les deux sont nécessaires.


---
#
## Évaluer pour l'apprentissage : le processus

Wiliam & Thompson (2007, cités par Lepareur 2016)

1. Clarifier, partager et faire comprendre les **intentions d’apprentissage et les critères de réussite**
2. Organiser de véritables **discussions, activités et tâches** qui produisent des preuves sur les apprentissages
3. Donner un **feedback** (rétroactions) qui fait progresser les élèves
> 4. Inciter les élèves à être des **personnes-ressources** pour leurs pairs
> 5. Inciter les élèves à **s’approprier leurs apprentissages**


---
# 
## Évaluer pour l'apprentissage : les rétroactions 
## :warning: Très important pour les apprentissages

Trois types de rétroactions, informantes pour l'élève (Hattie & Timperley 2007 ; voir aussi Lepareur 2016)

- Où est-ce que je vais ? (quels sont les buts ?)
- Comment est-ce que j'y vais ? (quels sont les progrès que j'ai faits vers ces buts ?)
- Que faire ensuite ? (quelles tâches dois-je réaliser pour mieux y arriver ?)

---
# 
## Rappel sur les rétroactions pour l'appentissage
* Nécessité de feedback
* Feedback, si possible, immédiat :arrow_right: biais de mémoire
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)

(Dehaene, 2018)


---
# 
# :two: Évaluer avec le numérique, c'est mieux ?



---
# Évaluer, une tâche chronophage

![](images/img-effets/evaluation_bryant.jpg)

Total en moyenne 49,5h, évaluation env. 15%

---
# Et avec le numérique ?
Le numérique permet de réallouer 20-30 % du temps hebdomadaire sur un total en moyenne 49,5h. (Bryant et al. 2020) :
* évaluation/rétroactions : gain 3 h



### C'est déjà un avantage, il y a en d'autres selon les utilisations ...
## :warning: Mais il faut toujours s'interroger sur l'évaluation avec le numérique


---
## Numérique vs. papier-crayon ? :thumbsup:

- peut recueillir des **processus** (durées, hésitations, etc.) analysables par la suite
- traitement **automatique**, donc moins de "fatigue évaluative" côté enseignant, plus d'entraînement côté élèves
- attribue des niveaux de manière plus **fiable et rapide**, reposant sur des statistiques
- peut proposer des **rétroactions** (semi-)automatiquement juste après la production
- offre des **visualisations synthétiques** pour suivre les progrès des élèves
- s'adapte aisément **au nombre** (2 fois plus d'évaluations < 2 fois plus de travail)
- peut être considéré comme **moins sujet aux stéréotypes** ou aux effets d'ancrage

---
## Numérique vs. papier-crayon   :thumbsdown:

- données plus aisément **piratables, modifiables**
- difficile de ne recourir qu'au libre et gratuit, donc **aspiration de données personnelles** difficile à contourner
- **plus de données** ≠ meilleure évaluation 

---
# 
## :warning: Attention aux fausses bonnes idées !

- fausse impression de **personnalisation** : les parcours automatisés peuvent enfermer l'élève dans des "profils"
- fausse impression d'**objectivité** : si on scrute les mauvaises choses, on obtient une mauvaise évaluation
- fausse impression d'**exhaustivité** : avoir beaucoup de données sur l'apprentissage nécessite beaucoup de temps pour l'analyser

---
# 
# :three:  Pratique

---
#  :pencil2: TP : Évaluation numérique 

Par groupe, vous allez essayer de déterminer et analyser **une forme** d'évaluation ou d'auto-évaluation **numérique** réaliste que vous utilisez ou que vous pourriez utiliser dans vos classes.


**Production attendue :** une présentation de 3 à 5 minutes par groupe avec **une seule** diapo au format PDF.

Temps de préparation : 40 minutes maximum

---
## Contenu de la présentation :
* Description de la situation d'évaluation ou d'auto-évaluation (niveau, discipline, contexte), type d'évaluation (positionnement, régulation, certification...)
* Compétences et connaissances **précises** qui seront évaluées.
:arrow_right: Processus cognitifs scrutés (savoir, réflexion, construction d'un raisonnement...)
* Outil(s) numérique(s) utilisé(s)
* Quelles rétroactions (feed-back), vers l'apprenant, l'enseignant, l'administration ?
* Quelles différences avec une évaluation non numérique ? (bénéfices/contraines)

**Rappel:** Une seule diapo au format PDF !





##### :warning: Vous êtes en formation = les erreurs et approximations sont normales, pas de stress !
---


# Présentations
(40 minutes Maximum)


---

# 
# Évaluer avec le numérique
# OU
# Évaluer au numérique
  
---
# Évaluer au numérique
Le numérique, des compétences à évaluer
* Un cadre de référence : [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques)
* S’applique à tous les niveaux de l’école
* :warning: Aucun niveau exigé par l'institution mais scruté par certains employeurs
* Un outil d’évaluation national : [PIX](https://pix.fr)
* Auto-évaluation des élèves
* Auto-formation (tutoriels)
* Certification d’un niveau d’acquisition : 3ème, Terminale, Université.

---
# 
## PIX : Évaluation et certification de compétences d'usages des technologies numérique.


### Allez sur le site pix.fr

---
# Évaluer au numérique : PIX
## :pencil2: TP - Découverte rapide de PIX
* Si vous n'en n'avez pas encore, créez-vous un compte sur pix.fr
* Balayez le référentiel et repérez quelques compétences que vous pourriez travailler dans vos disciplines
* Effectuez quelques tests

---
# Information PIX+EDU
![](images/img-effets/logo_pix_edu_small.png)

* Nouvelle certification (en phase expérimentale, généralisation prévue en septembre 2023)
* Objectif : certifier les compétences "métiers" des professeurs 
* Certification en deux volets
* Obligation à partir de la rentrée 2023 ? :arrow_right: Pas de texte officiel à ce jour
* Référentiel et critères d'évaluation non validés à ce jour



---

![](images/img-effets/pix_edu_p1.png)

---

![](images/img-effets/pix_edu_p2.png)

---

![](images/img-effets/PIX_eformation.png)

---
# Conclusion

La recherche indique que le numérique ce n'est pas magique pour les apprentissages, en revanche le numérique peut apporter de réelles plus-values en termes d'évaluation.




---
# Bibliographie et ressources


* Bryant, J., Heitz, C., Sanghvi, S., & Wagle, D. (2020). How artificial intelligence will impact K-12 teachers. New York: McKinsey & Company.

* Coen, P.-F., Detroz, P., & Younès, N. (2020). Introduction Évaluation et numérique : des pratiques éclectiques qui explorent des espaces à déchiffrer. Évaluer. Journal international de recherche en éducation
et formation, 5(3), 1-8.

* De Ketele, J.M. & al. .1989. Guide du formateur, Pédagogie en développement. De Boeck : Université.

* De Ketele, J.M., Gérard, F.M., Roegiers, X. (1997). L'évaluation et l'observation scolaires : deux démarches complémentaires, Éducations - Revue de diffusion des savoirs en éducation, 12, 33-37.

---

* Hattie, J., & Timperley, H. (2007). The Power of Feedback. *Review of Educational Research*, *77*(1), 81–112. doi:10.3102/003465430298487

* Ladage, C. (2020) Fun-mooc : Évaluer avec le numérique partie I – De quoi parle-t-on?.
 accédé le 14 juin 2022 à https://lms.fun-mooc.fr/c4x/ENSCachan/20003/asset/EFANS6_1_evaluation-de-quoi-parle-t-on.pdf

* Lepareur, C. (2016). *L’évaluation dans les enseignements scientifiques fondés sur l’investigation : Effets de différentes modalités d’évaluation formative sur l’autorégulation des apprentissages*. (Doctorat de sciences de l'éducation). Univ. Grenoble Alpes, Grenoble. 

* McDonald, R., Boud, D., Francis, J., & Gonczi, A. (2000). New perspectives on assessment (Vol. 4). Paris : UNESCO, accédé le 23 avril 2020 à https://unesdoc.unesco.org/ark:/48223/pf0000101855

---

* Pellegrino, J. W. (2013). Measuring what matters. Technology and the design of assessments that support learning. In R. Luckin, S. Puntambekar, P. Goodyear, B. Grabowski, J. Underwood, & N. Winters (Eds.), *Handbook of design in educational technology* (pp. 377–387). New York: Routledge.

* Scriven, M. (1991). *Evaluation thesaurus* (4th ed.). London: SAGE.

* Stufflebeam, D. L., Foley, W. J., Gephart, W. J., Guba, E. G., Hammond, R. L., Merriman, H. O., & Provus, M. M. (1980). L'évaluation en éducation et la prise de décision.* Ottawa: N.H.P.

* Wiliam, D., & Thompson, M. (2007). Integrating assessment with instruction: What will it take to make it work? In C. A. Dwyer (Ed.), *The future of assessment: Shaping teaching and learning* (pp. 53–82). Mahwah: Erlbaum.




