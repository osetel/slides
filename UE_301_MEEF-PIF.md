---
marp: true
autoscale: true
theme: marp

---
<!-- _paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 301 Évaluer l'apprentissage par ordinateur : Tâches et rétroactions
#### Master 2 MEEF-PIF, Année 2023-24

### [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes & Nelly Tarbouriech, Rectorat de l'académie de Grenoble
![w:350](images/logo-espe-uga.png)

---
<!-- paginate: true -->
<!-- footer: Ph. Dessus & N. Tarbouriech | UE 301 MEEF-PIF, Inspé UGA | CC:BY-NC-SA | 2023–24 -->

# Séance 1 (14 septembre 2023)

- Présentation de l'organisation du cours
- Présentation de quelques réalisations des années antérieures
- Première réflexion sur le domaine du cours interactif

---
# Objectifs de l'UE

À l’issue de l'UE, les étudiant·e·s seront capables :

- d’analyser un **domaine de connaissances** pour en dégager des concepts, pouvant faire l’objet d’une évaluation automatisée ;
- de connaître les différents **types de rétroactions** et d’en concevoir qui soient appropriées au niveau de connaissances de l’élève ;
- de réaliser un **cours interactif** composé de questions évaluatives, avec un outil de création de QCM et/ou de cours interactifs.

---
# Organisation du cours (1/2)

Enseignement “inversé” : tout le cours est disponible pour lecture dès le début. Du travail est à réaliser à distance (**ne pas hésiter à solliciter les enseignant.es par courriel même en dehors des semaines de cours**).
1\. **Discussion sur le cours lu auparavant (et les activités réalisées)**. Chaque séance dédiée à un approfondissement du cours lu au préalable :
	- Qu’est-ce que j’ai compris du cours ?
	- Qu’est-ce qu’il me reste à comprendre ?
	- Y a-t-il dans le cours des choses à préciser, développer ? des inexactitudes ?
	
---
# Organisation du cours (2/2)

2\. **Travail dirigé en binômes** : les TD sont de petits projets, donnant lieu à diffusion en cours, permettant d'avancer le projet du cours interactif
3\. **Réalisation individuelle d'un cours interactif** :  La partie la plus importante de l'UE est la réalisation du cours interactif avec un logiciel spécifique

:warning: Nécessité d'amener un ordinateur portable à chaque séance (ou *a minima* à celle où l'on travaillera sur le projet de cours interactif)

---
# Commentaires

- Chaque séance fait l'objet d'un compte-rendu diffusé à tous (par les étudiants) après vérification des enseignants (porté sur la plate-forme d'enseignement)
- Peut être suivi à distance, mais nécessite dans tous les cas un travail suivi, donc privilégier le suivi en présence
- Compétences en informatique : standard, mais avec une volonté d'approfondir les questions du cours

---
# Supports de cours

- [https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/) (cours complet)
- [https://eformation.univ-grenoble-alpes.fr/course/view.php?id=1027](https://eformation.univ-grenoble-alpes.fr/course/view.php?id=1027) (cours plate-forme Moodle) nécessitant l'auto-inscription des étudiants avec une clé : **301MEEFPIF** 
- Les étudiant·e·s muni·e·s d'un ordinateur se connectent au site avec leur identifiant étudiant et saisissent la clé
- La plate-forme contient : les documents et présentations diffusés en cours, les comptes-rendus de séance rédigés par les étudiant·e·s


---
# Modalités de travail

- Un suivi du cours à distance est possible s'il vous est impossible de le suivre en présence (nécessite un travail régulier)
- À partir de l'année universitaire 2023-24, les étudiants travailleront, dans cette UE et dans l'UE “Scénarisation et suivi des apprentissages”, **sur le *même* cours interactif** ; cela de manière à réduire leur charge de travail (leur évaluation portera sur des éléments distincts).
- :warning: **Ne pas sur-travailler** : Ne consacrer env. qu'une 1 h 15 (± 30 min) au travail à la maison
- Vous aurez à choisir **l'un des deux logiciels** pour construire un cours : eXe Learning ou Lumi/H5P (des infos seront données ultérieurement)
- :warning: Ne pas hésiter à signaler à l'enseignant toute erreur dans le cours, ou passage peu clair

---
# Début du travail sur le contenu du cours (1/2)

Ce qui suit est tiré du doc. de cours [fin du syllabus](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/syl-organisation-MEEFPIF.html). Commencer à réfléchir à un "contenu de cours" :

- domaine et public au choix (à bien définir *a priori*)
- préférablement **en lien avec vos préoccupations extérieures** (cours à faire), et/ou lié à une autre UE

---
# Début du travail sur le contenu du cours (2/2)

- correspond à un document nécessitant env. **1 h de lecture** (env. 5 p A4 ou 10 000 caract. esp. compris, 2 niveaux de hiérarchie), permettant une bonne compréhension des concepts en jeu (QCM ajouté ultérieurement)
- exempt de plagiat, mais l'utilisation de manuels est conseillée
- commencer à placer les notions sur une carte de concepts avec liens (Fig. 1, doc. [conception](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_proc_conception.html))


---
# :warning: Usage de robots conversationnels :warning:

Ce cours a été conçu pour vous aider à développer des connaissances et à acquérir de nouvelles compétences qui vous seront utiles en tant que professionnels. Les outils d'IA peuvent être utilisés comme une aide au processus créatif, mais il est entendu qu'ils doivent être accompagnés d'une pensée critique et d'une réflexion. **Les étudiants qui choisissent d'utiliser ces outils sont responsables de toute erreur ou omission résultant de leur utilisation**. Il leur sera également demandé de fournir en annexe du dossier le nom de l’outil utilisé, les prompts utilisés, l'historique des résultats générés, ainsi qu'une réflexion approfondie sur ces résultats (notamment sur leur validité). Le cas échéant, les étudiants peuvent également être invités à examiner les coûts environnementaux et sociaux de l'utilisation des outils.

---
# 1. Quelques exemples de productions

:warning: Les liens ci-dessous sont internes à l'ordinateur de l'enseignant, donc ne pourront être ouverts depuis le PDF diffusé.

- [Apprendre à porter secours, Cycle 3](hook://file/HeFcSTI1L?p=QXJjaGl2ZS9TZcyBdmVyaW5lIEJsYW5jaG9u&n=Apprendre%20a%CC%80%20porter%20secours%20version%20finale%2Ehtml)
- [La circulation sanguine, Cycle 4](hook://file/HeG3OJT5F?p=QXJjaGl2ZS9BdWRyZXkgRGVzbWFycw==&n=LA%20CIRCULATION%20SANGUINE3%2Ehtml)
- [Le devenir de la matière organique, Cycle 3](hook://file/HeG8ypynC?p=QXJjaGl2ZS9TdGXMgXBoYW5pZSBBZ3Jlc3Rp&n=agresti%5Fmatiere%5ForganiqueVF%2Ehtml)
- [Les campagnes au Moyen-Âge, 5e](hook://file/HeGBsc9A3?p=QXJjaGl2ZS9BZGFtIER1cGVycm9u&n=DUPERRON%20Adam%20M2%20MEEF%20PIF%20AE%20DESSUS%2Ehtml)
- [Problèmes ouverts, professeurs](hook://file/HeGI95lQ5?p=QXJjaGl2ZS9MYXVyZW5jZSBNb3NzdXo=&n=Proble%CC%80mes%20ouverts%2Ehtml)
- [Notion de fonction, 3e](hook://file/W3XkqKLss?p=UElGLU1FRUYyMy9XYWxnZW53aXR6IEdhZcyIbGxl&n=UE301%20%2D%20Notion%20de%20fonction%20%2D%20G%20WALGENWITZ%2Ehtml#h5pbookid=2614211806&chapter=h5p-interactive-book-chapter-49f8abd3-4002-4a97-82af-1d81148bb8e0&section=0)

---
# 1. Pour la prochaine séance

- Visionner la conférence de Ph. Dessus "[Les QCM font-ils apprendre ?](https://pod.grenet.fr/video/4125-atelier-n104-les-questionnaires-a-choix-multiple-font-ils-apprendre/)" 
- Faire l’atelier "[Les activités cognitives engagées dans un QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/atelier-qcm-cog.html)".


---
# Séance 2 (20 septembre 2023)

- Échange sur ce qui a été compris/non compris des deux exercices (QCM mémoire et conférence sur les QCM)
- Lire le document "[Des tâches pour évaluer les connaissances des élèves](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/tache_eval.html)", notamment le Tab. 1.

---
# Séance 2 (suite)

- Choisir, **par binôme**, une activité citée dans la section "Analyse de tâches", lire attentivement l'article la concernant et servez-vous des informations qu'il contient pour réfléchir et augmenter la ligne du Tab. 1. P. ex. en détaillant 
	- les buts d'enseignement liés
	- les contextes dans lesquels la tâche d'évaluation peut être utilisée,
	- ses avantages et inconvénients (du point de vue de l'enseignant ou des apprenants).
	- ce que l'informatique peut apporter dans l'activité
- Compte rendu oral en fin de séance.

---
# 2. Pour la prochaine séance

- Livrer le travail du TD 1
- Lire les documents :
	- [Les rétroactions : définition et rôle dans l’apprentissage](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/retro_defs.html)
	- [Les rétroactions informatisées](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/retro_info.html) 
	- [Un processus de conception de cours avec QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_proc_conception.html)

<!--
---
# Séance 3 – Analyse de contenu (28 septembre 2022)

- Échange sur ce qui a été compris/non compris à propos des documents à lire :
	- Les rétroactions : définition et rôle dans l’apprentissage
	- Les rétroactions informatisées 
	- Un processus de conception de cours avec QCM
- Commentaires rapides sur le TD

---
# Séance 3 - TD

- Avancer ou finir le TD 1 (30 min)
- Suivre le doc. “Processus de conception de cours”, 
	- étape 1 : finir la carte de concepts
	- étape 2 : identifier les unités de connaissances
- Faire relire par l'enseignant

---
# Séance 3 

- Pour la Séance 4  du 5 octobre
- Lire les Documents 
	- [Les différentes formes de rétroactions](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/retro_formes.html), 
	- [Les QCM : Bref historique](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_historique.html), 
	- [Les questionnaires à choix multiple : définitions et critiques](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_def.html),
	- [Les différents formats de QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_formes.html).
- Installer *eXe Learning* ou *Lumi* sur votre ordinateur et commencer à l'utiliser
- Envoyer à l'enseignant le TD 1 (act. d'éval.) pour relecture

--- 
# Séance 4 (5 octobre 2022) 1/4

- Échange sur ce qui a été compris/non compris à propos des documents :
	- Les différentes formes de rétroactions, 
	- Les QCM : Bref historique, 
	- Les questionnaires à choix multiple : définitions et critiques,
	- Les différents formats de QCM.
- Point sur l'installation eXeLearning et Lumi : problèmes rencontrés ? Quelques éléments de choix

---
# Séance 4 (2/4)

- Activité du TD :

1. Rédigez, à  propos du thème de votre cours interactif, 3 items de QCM complets (comprenant chacun 1 amorce et 4 choix, dont 1 seul exact)
2. Lisez *ensuite* le document "[Rédigez des items de QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_principes.html)", et commencez à rendre à vos 3 items un format plus approprié, pour le modifier pas à pas, en précisant : 
	- quelle règle a été appliquée, 
	- les modifications réalisées, 
	- quelles rétroactions afficher en fonction des réponses. 


---
# Séance 4 (3/4)

- Pour la séance 5 (avec Nelly Tarbouriech)
	- poursuivre le travail commencé en séance 4 (Items de QCM)
	- m'envoyer le TD 2 "analyse de contenu" (cartes de concepts et unités de connaissance)  pour retours
---
# Séance 4 (4/4)

- Activité pouvant être menée à plusieurs, 2 ou 3 max) : 
	- Prendre connaissance d’usages pédagogiques de situations d’apprentissage incluant l’utilisation de QCM (lire le document "[Quelques usages pédagogiques des QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_principes.html)") ; 
	- observer quelques pratiques et types d’activités parmi celles proposées (dans la section Situations d’apprentissage proposées) et 
	- analyser une situation en vous aidant de la [Grille d’observation de pratiques](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/usages-peda.html#grille-obs-pratiques). 
	- chaque groupe consigne son analyse dans le pad https://annuel.framapad.org/p/masterpif_qcmusagespedagogiques


---
# Séance 6 (19 oct. 2022)

- Échange sur ce qui a été compris/non compris à propos du document à lire pour aujourd'hui :  “Répondre à un QCM : Aspects cognitifs" et "Les rétroactions, leur réception par  les élèves".
- Travail individuel sur le logiciel de création de cours interactifs
- Livraison par courriel du TD 3 sur les items de QCM (au plus tard vendredi de cette semaine)


---
# Travail de la séance

- Continuer de rédiger le contenu du cours
- Commencer à le “rentrer” dans le logiciel de création de cours interactifs choisi
- D'ici à la semaine prochaine (le cas échéant) : livrer le TD sur le QCM. 
- Pour la séance suivante : avancer dans le cours interactif

--- 
# Séance 8 du 23 novembre 2022

- Échange sur ce qui a été compris/non compris à propos du document à lire pour aujourd'hui :  “Répondre à un QCM : Aspects cognitifs" et "Les rétroactions, leur réception par  les élèves".
- Présentation rapide des 2 sujets de stage proposés par Ph. Dessus
- Séance dédiée à la réalisation du projet, chacun·e finalisant une version “présentable” pour la séance prochaine, où les projets, même s'ils sont encore en travaux, seront présentés à tous
- :warning: Date-butoir de livraison de l'ensemble des documents de l'évaluation le **le dimanche 15 janvier 2023 à 23:59**.

---
# Sujet 1 L'utilisation de la recherche fondée sur les preuves en PU

**Tuteur de stage** : Daniel Seyve, DAPI 

- **But général** : développer des pratiques de formation en pédagogie universitaire (PU) fondées sur les preuves.

1. Analyser les raisons pour lesquelles les formateurs intervenant en PU ont des difficultés à prendre en compte les résultats des recherches en PU pour concevoir et réaliser des formations dans leur domaine. 
2. Faire un inventaire des notions et résultats de recherche en PU et le proposer aux formateurs de la DAPI-UGA.

---
# Sujet 1. (2/2)

3. Réaliser des entretiens pour réondre aux questions suivantes :

- quelles références à la recherche sont réalisées dans les formations existantes de la DAPI ? À quel niveau peuvent-elles être classées ?
- quels sont les plus importants types de documents académiques à analyser pour arriver à construire des prescriptions dans le domaine de la PU ?
- les éléments théoriques et pratiques issus de l'EFP peuvent-ils être formulés de manière compréhensible et transférables aux différents contextes des formés ? On restreindra la question à un thème de recherche spécifique ;
- quel est le format idéal de ces éléments, et ce format peut-il être standardisé *a minima* pour être reporté sur le site de la DAPI ?
- quelle restructuration du catalogue de formation peut-il en résulter ? Quelles nouvelles formations pourraient être conçues ?
- quelles activités pédagogiques peuvent permettre aux formés de mieux s'approprier ces éléments théoriques ? 

---
# Sujet 2. L'utilisation de recherches sur l'éducation à l'esprit critique
 
**Tuteur de stage**  : Pleen Le Jeune (LPI, Univ. Paris-Cité)

Le développement de l'esprit critique (EC) des jeunes fait partie des programmes scolaires et est jugé important par les enseignant·es mais environ un·e enseignant·e sur 5 ou sur 6 seulement se sent adéquatement outillé·e et formé·e pour cela. Grande diversité des définitions, des approches éducatives, et des moyens de l'évaluer

De nombreux facteurs pouvant faciliter ou compliquer l'utilisation de recherches par les enseignant·es ont été identifiés (Dagenais *et al*., 2012). En s'inspirant de ces recherches, nous souhaitons proposer une approche de science citoyenne qui permettra à de nombreux·ses acteur·rices de l'éducation de collaborer pour faciliter l'utilisation de recherches des enseignant·es spécifiquement sur l'éducation à l'EC.

---
# Sujet 2. Contexte du travail 
L'association [ÉPhiScience](https://ephiscience.org/) a réalisé une synthèse des recherches récentes sur l'éducation à l'esprit critique en décembre 2020. Fin octobre 2022, elle a lancé un chantier de réécriture plus large qui viserait à transformer cette synthèse de sorte à ce qu'elle puisse davantage faciliter l'utilisation de recherches sur l'éducation à l'esprit critique par des enseignant·es. 


---
# Sujet 2. Grandes étapes du chantier

De novembre 2022 à août 2023 :

**1) Relecture et prise de notes (novembre-février)** : Quelles  actions d'éducation à l'EC déjà menées, quelles idées d'actions inspirées de la lecture de la synthèse. Ces éléments serviront de base pour la prochaine étape.

**2) Description de Défis et d'Actions concrètes cohérentes avec la synthèse (décembre-avril)** : Décrire des défis et des actions concrètes sur la plate-forme profs-chercheurs 

**3) Hackathon - définition du processus de réécriture** (**avril-mai**) :  Co-définition du  processus de réécriture de la synthèse en vue d'arriver à un document final. 

**4) Réécriture de la synthèse (mai-août)** : À partir de tout ce processus, l'équipe des co-auteur·rices mènera à son terme la réécriture finale de la synthèse.

---
# Sujet 2. Tâches dans le cadre du stage

En fonction de l'intérêt de la personne stagiaire, nous pourrons proposer de travailler sur plusieurs éléments des trois premières tâches :
- L'analyse et le résumé des notes de relecture des différentes personnes mobilisées (enseignant·es, chercheur·euses, etc.) ;
- L'analyse des défis et des actions émergeant lors de la deuxième étape ;
- La co-conception et l'animation des ateliers du hackathon ;
- L'analyse réflexive de la démarche dans son ensemble dans une perspective de facilitation de l'utilisation de recherches en éducation.

---
# Séance 9 du 30 nov. 2022

Déroulement :

- point sur la livraison du contrôle continu
- chacun présente le but de son cours interactif  et en visionne par vidéo-projecteur quelques pages 
- questions et commentaires de l'assistance, pouvant être pris en compte pour l'élaboration de la version finale du cours

---
# Livraison et contenu
- date-butoir de livraison de l'ensemble des documents le **le dimanche 15 janvier 2023 à 23:59**

---
# Documents à livrer (les TD dans un seul PDF) (1/2)
À envoyer par courriel à Ph. Dessus (réception sera confirmée), qui transmettra à N. Tarbouriech
- Le **TD 1** séances 2-3 (analyse tâche d’apprentissage et évaluation) (env. 1 page).
- Le **TD 2** séance 4 (carte de concepts, “grains de connaissances”) (env. 3 pages).
- Le **TD 3** séance 5 (items de questions avant/après) (env. 1 page).
- Le **TD 4**  séance 7 (Carte mentale sur les apports des QCM)

---
# Documents à livrer  (2/2)
- Le cours interactif (en format HTML zippé, 
	- **Fichier>Exportation>Site Web** dans *eXe Learning*  ; 
	-  **Fichier>Exporter>FIchier HTML tout en un** dans *Lumi* ; si vous avez de nombreux fichiers vidéo choisir l'option **Fichier>Exporter>Fichier HTML et plusieurs fichiers multimedia** et zippez l'ensemble) ; 
	- ou à défaut, l'URL du cours mentionnée dans une page (pour les cours réalisés dans des systèmes en ligne). 
- Envoyer les cours zippés > 5 Mo par FileSender http://filesender.renater.fr ou PIA>envoi de fichiers lourds (rectorat). :warning: : certains anti-virus refusent l'envoi par courriel

---
# Critères d'évaluation
* TD 1 (tâche éval) : **2 points**
* TD 2 (carte grains) : **2 points**
* TD 3 Items (évalués dans le cours interactif)
* TD 4 Carte mentale : **2 points**
* Qualité du cours : **4 points**
* Qualité des QCM : **5 points**
* Qualité des rétroactions : **4 points**
* Forme : **1 point**

---
# Merci de votre travail et engagement durant cette UE !

-->