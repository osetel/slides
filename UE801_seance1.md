
![20%](images/img-intro-801/logo_UGA_couleur_cmjn.jpg)


# Culture numérique et Apprentissage

## UE - Num 801 
Equipe formateurs numérique - UGA - 2023

<!-- page_number: true -->
<!-- footer: Equipe formateurs numérique 2023 - CC:BY-NC-SA -->

---
# L'U.E. Num 801, c'est quoi?
* Des contenus théoriques sur les usages du numérique en situation d'apprentissage
* Des notions de littératie numérique
* Un travail d'analyse et de prospective sur l'usage du numérique en situation d'apprentissage

---
# Pourquoi?
* 	Pour être capable de se poser des questions sur les plus-values supposées des situations d'apprentissage intégrant le numérique lors de leur conception.
* 	Pour réduire le plus possible les risques juridiques, éthiques, physiologiques et effets négatifs en termes d'apprentissages induits par les usages du numérique.
* 	Pour être capable d'analyser objectivement une situation d'apprentissage déjà réalisée et pouvoir en proposer une ou des évolutions bénéfiques pour les élèves.

---

# Modalités et attendus
---
## Séances 1 à 5, apports thématiques 
:warning: Ordre aléatoire, formateur différent

* Evaluer et s’auto-évaluer avec le numérique
* Production et usages de ressources pédagogique numériques
* Numérique et juridique
* Enseigner avec le numérique : quelques zones à risque
* Effets du numérique sur les apprentissages 


---
## Séances 6, 7 et 8 : “Analyse d’une situation d’enseignement-apprentissage”
* La séance 6 comprend une brève évaluation des connaissances liées aux thématiques, puis les étudiants, par groupes de 2, choisissent une situation issue de leur expérience, d’observations, ou de recherches, et la décrivent.

* La séance 7 est consacrée à un travail d’analyse mettant en application les apports des 5 thématiques à la situation.

* La séance 8 est consacrée à une mise en commun des différentes analyses, pour les mutualiser et les améliorer par la discussion.

---
# Evaluation

L’évaluation est sous la forme d’un contrôle continu. Elle sera constituée des deux évaluations suivantes :

* Evaluation de connaissances en séance 6 sur tout ou partie des apports théoriques, sous la forme d’un test automatisé (durée 30 min).
* Evaluation d’un dossier de 6 à 7 pages constitué en binôme au cours des séances 6, 7 et 8. La production sera déposée sur la plateforme pour évaluation **au plus tard le 25 mars 2023 à 23:59**



---
# Connexion à la plate-forme



* Adresse : <https://eformation.univ-grenoble-alpes.fr/>
* Connexion par « compte universitaire »

| Page d'accueil | Page de connexion |
| :-------------: | :---------: | 
|![40%](images/img-intro-801/Moodle1.png) |![30%](images/img-intro-801/moodle18.png) |


---
### Choix de l'établissement
Université Grenoble Alpes ou Université de Savoie
![50%](images/img-intro-801/Moodle2.png)

Authentification avec ses identifiants universitaires
![50%](images/img-intro-801/Moodle4.png)

---
## Recherche du cours
Retour à l'accueil du site
![50%](images/img-intro-801/Moodle5.png)
Descendre dans la page pour accéder au champ de recherche des cours
![50%](images/img-intro-801/Moodle6-M1-SD.png)
Sélectionner le cours Num801 : UE Culture numérique et apprentissage second degré
![40%](images/img-intro-801/Moodle7-M1-SD.png)

---
## Inscription par clé 
### Fournie par le formateur
![50%](images/img-intro-801/Moodle8-M1-SD.png)

Si vous vous êtes trompé-e de groupe, vous pouvez vous désinscrire et rentrer la bonne clé d'auto-inscription
![50%](images/img-intro-801/Moodle9.png)

---
