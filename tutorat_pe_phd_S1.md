---
marp: true
paginate: true
autoscale: true
theme: uga

---
# Séances tutorat classes
## Inspé, Univ. Grenoble Alpes
## Philippe Dessus
## Séance 1 — 10 novembre 2022


<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)



---
<!-- page_number: true -->
<!-- footer:  Dessus Ph. – Tutorat M2 – 2022 - CC:BY-NC-SA  -->

# Présentation rapide
- prénom ; niveau de classe de stage ; caractéristiques principales de l'école, de la classe

---
# Syllabus des séances de tutorat 

https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/syl-tutorat-inspe.html

---
# Préambule (1/2) : Règles de fonctionnement du groupe
- Une partie du travail de l’atelier est fondée sur la confrontation entre participants de leurs pratiques de classe, leur compréhension et leur amélioration. Il s’agit de les rapporter le plus précisément possible et, en contre-partie, d’éviter de porter des jugements sur la personne qui a rapporté les pratiques, mais d’évaluer le plus objectivement possible ce qui s’est passé (et non la personne qui est à l’origine possible de ces pratiques).
- Ce travail d’analyse des pratiques peut être outillé par le CLASS (un outil d’observation de la qualité des interactions enseignant-élèves). Le formateur et les participants mettront tout en œuvre pour permettre une parole libre, positive et constructive à propos des pratiques de chacun.

---
# Préambule (2/2)

- Le résultat final de l’atelier mémoire fera l’objet d’une évaluation, et non les pratiques relatées ou visionnées au cours de l’atelier.
- Les propos tenus au sein du groupe reposent sur la **confidentialité **(c.-à-d. que les participants ne peuvent rapporter ce qui s’est dit dans l’atelier à des personnes n’en faisant pas partie) et le respect de la parole de chacun des membres.
- Le CLASS n'est pas un outil de l'évaluation de l'enseignant·e, mais bien de ce qui se joue entre l'enseignant et ses élèves

---
# Le rôle de CLASS
- CLASS est un outil d'observation de la qualité des interactions enseignant·e/élèves
- Nous allons l'utiliser plutôt, dans ces séances de tutorat, comme un outil
	- permettant d'avoir un vocabulaire commun
	- permettant de vous centrer sur des pistes d'analyse de situations et d'amélioration
	
---
# Revue rapide du système CLASS
Pour s'assurer d'une bonne compréhension

---
# Domaine 1 – Soutien émotionnel
- Des relations chaleureuses, soutenantes avec les adultes et les autres enfants
- La joie et le désir d'apprendre
- Une motivation à s'engager dans les activités d'apprentissage
- Un sentiment de confort (aisance) dans le milieu éducatif
- La volonté de relever des défis sur le plan social et académique
- Un niveau approprié d'autonomie

---
# Soutien émotionnel - 1 Climat positif (C+)
Reflète le lien émotionnel entre l'enseignant et l'enfant, entre les enfants eux-mêmes ainsi que la chaleur, le respect et le plaisir communiqué dans les interactions verbales et non-verbales

**Indicateurs**
- Relations
- Affect positif
- Communication positive 
- Respect

---
# Soutien émotionnel - 2 Climat négatif (C–)
N'est pas simplement l'absence de climat positif mais bien la présence de comportements négatifs. Reflète l'ensemble de la négativité exprimée dans la classe. La fréquence, l'intensité et la nature de la négativité de l'enseignant et des enfants sont des éléments-clés de cette dimension

**Indicateurs**
- Affect négatif
- Contrôle punitif
- Sarcasme/Irrespect
- Sévère négativité

---
# Soutien émotionnel - 3 Sensibilité de l'enseignant (SE)
Englobe la capacité de l'enseignant à être attentif·ve et à répondre aux besoins émotionnels et académiques des enfants. Un haut niveau de sensibilité offre un soutien aux enfants dans l'exploration et l'apprentissage puisque l'enseignant leur offre de manière constante du réconfort et de l'encouragement

**Indicateurs**
- Conscience/vigilance
- Réceptivité
- Réponse aux problèmes
- Confort des élèves

---
#  Soutien émotionnel - 4 Prise en considération du point de vue de l'enfant (PVE)
Permet de rendre compte dans quelle mesure les interactions de l'enseignant avec les enfants et les activités offertes mettent l'accent sur les intérêts des enfants, leur motivation et leur point de vue tout en encourageant la responsabilité de l'enfant et son autonomie

**Indicateurs**
- Souplesse et attention centrée sur l'élève
- Soutien à l'autonomie et au leadership
- Expression des élèves
- Restriction de mouvement

---
# Domaine 2 – Organisation de la classe
Comment l'enseignant organise et gère différents aspects de la classe (comportement des élèves, temps, attention). Il y a une meilleure opportunité d'apprentissage et la classe fonctionne mieux si les élèves ont un comportement adéquat, cohérent avec les tâches qui leur sont allouées, et sont intéressés et engagés dans ces dernières.

---
# Organisation classe - 5 Gestion des comportements (GC)
Englobe l'habileté de l'enseignant à établir des attentes comportementales claires et à utiliser des méthodes efficaces pour prévenir et rediriger les comportements inappropriés

**Indicateurs**
- Attentes comportementales claires
- Proactivité
- Redirection des comportements
- Comportements des enfants

---
# Organisation classe  – 6 Productivité (P)

Dans quelle mesure l'enseignant gère le déroulement des activités et des routines de manière à optimiser le temps disponible à l'apprentissage ?

**Indicateurs**
- Maximisation du temps d'apprentissage
- Routines
- Transitions
- Préparation du matériel nécessaire à la séance

---
# Organisation classe - 7 Modalités d'apprentissage (MA)

Reflète la manière dont l'enseignant maximise l'intérêt des enfnats, leur engagement et leur capacité à apprendre au cours des leçons et activités offertes

**Indicateurs**
- Accompagnement efficace
- Diversité des modalités et des matériels
- Intérêt de l'enfant
- Clarté des objets d'apprentissage

---
# Domaine 3 – Soutien à l'apprentissage

S'intéresse à décrire comment les enseignants aident les enfants à 
- apprendre à résoudre des problèmes, à raisonner et à penser
- utiliser les rétroactions pour approfondir des habiletés et des connaissances
- développer des habiletés langagières plus élaborées

---
# Soutien appr — 8. Développement de concepts (DC)

Utilisation par l'enseignant de stratégies diverses (discussions, activités) afin d'amener l'enfant à développer une pensée plus élaborée, une plus grande compréhension des phénomènes de son environnement plutôt que de favoriser un apprentissage par cœur

**Indicateurs**

- Analyse et raisonnement
- Créativité
- Intégration d'éléments déjà rencontrés dans les cours précédents
- Lien avec la vie réelle

---
# Soutien appr — 9. Qualité de la rétroaction (QR)

Le niveau selon lequel l'enseignant offre des rétroactions aux enfants qui optimisent l'apprentissage et qui favorisent une participation maintenue

**Indicateurs**
- Étayage
- Rétroactions en boucle
- Stimuler les processus de la pensée
- Fournir de l'information
- Encouragement et affirmation
  
---
# Soutien appr — 10. Modelage langagier (ML)

Rend compte de l'efficacité et de la quantité des techniques de stimulation du langage utilisées par l'enseignant.

**Indicateurs**
- Conversations fréquentes
- Questions ouvertes
- Répétition et extension
- *Self-talk* et *parallel-talk*
- Niveau de langage élaboré


---
# Tour de table (1/2)
- Quel est le domaine (ou des dimensions dans un domaine) pour lequel vous vous sentez le/la plus à l'aise ?
- Quel est le domaine (ou des dimensions dans un domaine) pour lequel vous pensez avoir des difficultés ?
- :warning: Ces 2 impressions sont autant causées par vous que par vos élèves
- Considérez chaque dimension et annotez les possibles points forts/faibles

---
# Tour de table (2/2)

- Choisir 2 “dimensions-projet” sur lesquelles vous allez tout spécialement travailler

---
# Liste des dimensions
1. Climat positif (C+)
2. Climat négatif (C–)
3. Sensibilité de l'enseignant (SE)
4. Prise en considération du point de vue de l'enfant (PVE)
5. Gestion des comportements (GC)
6. Productivité (P)
7. Modalités d'apprentissage (MA)
8. Développement de concepts (DC)
9. Qualité de la rétroaction (QR)
10. Modelage langagier (ML)

---
# Travail pour la suite

- Commencer un journal de bord dans lequel, jour par jour :
	- vous déterminez une dimension sur laquelle vous allez tout particulièrement vous focaliser pendant la journée
	- vous relisez de près les indicateurs de la dimension et vous remettez en tête votre comportement correspondant
	- tout au long de la journée, vous portez une attention toute particulière à la qualité de vos interactions avec les élèves selon cette dimension
	- le soir, vous prenez quelques notes de bilan sur la manière dont cette centration sur la dimension a été efficace, suivie d'effets chez les élèves. Et vous notez les éléments sur lesquels porter une attention particulière la prochaine fois que vous vous centrerez sur la dimension.
  
---
# Un exemple de choix

> Par exemple, Lucie a choisi deux dimensions-projets : “Développement conceptuel“ et “Gestion des comportements”. Elle a cours les lundis et mardis. Voici ses choix de dimensions pour 4 semaines successives :
> - S1. Lundi : Développement conceptuel ; Mardi : Qualité des rétroactions.
> - S2. Lundi : Gestion des comportements ; Mardi : Modalités d'apprentissage.
> - S3. Lundi : Développement conceptuel ; Mardi : Climat positif.
> - S4. Lundi : Gestion des comportements ; Mardi : Productivité
>

---
# Séance 2 — 22 mars 2023

But de la séance :

- Faire le point sur sa pratique et les rétroactions du tuteur
- Réfléchir sur quelques dimensions du CLASS (notamment sur la dimension “Développement conceptuel“)
- Quelques principes pour la suite !

---
# Pour commencer...

- Lister 3 points sur l'enseignement que vous avez compris grâce aux stages
- Lister 3 points sur l'enseignement qu'il vous reste à comprendre

en reprenant (ou non) les dimensions du CLASS

---
# Bref compte-rendu/bilan

---
# Nuage de mots des rapports de visite

![w:800](images/img-tutorat/nuage-tutorat-23.jpg)


---
# Quelques constantes des observations

1. Enseignant.es à l'aise avec les élèves, sensibles, leur laissant de l'autonomie
2. Gestion des comportements non problématique, mais parfois un peu trop réactive, intégration de l'éventuel système à points à parfaire
3. Productivité qui pourrait parfois être améliorée, notamment à l'entrée en classe
4. Modalités d'apprentissage de bon niveau général, même si les buts des activités mériteraient d'être mieux explicités
5. Le développement de concepts demande à être travaillé et pensé en amont
6. Rétroactions à personnaliser et rendre plus spécifiques
7. Bon niveau de modelage langagier, même si les questions ouvertes pourraient être plus fréquentes

---
# Une vidéo : Arts plastiques CE1-CE2

[Accès : Canopé BSD, avec identification](https://www.reseau-canope.fr/BSD/sequence.aspx?bloc=886114)


---
# Cotation CLASS sur quelques dimensions (1/2)
(E)lèves ; (M)aîtresse


* **Modalités d’apprentissage \: 7 (++++)**. Grande diversité et richesse du matériel utilisé et des médias pédagogiques (*paper-board*, tableau blanc, ardoise, vidéo-projecteur). Les tâches sont très clairement expliquées (oralement et sur tableau blanc). Les E sont engagés.
	
* **Développement de concepts \: 7 (++++)**. M fait préciser les actions des élèves, évoque des séances précédentes, et le monde des arts. Le volet créativité est riche (être capable de créer une œuvre ; leur donner un nom). Il y a des épisodes d'analyse et de raisonnement à partir des œuvres (faites par les élèves ou présentées). Plusieurs références à du travail antérieur ("Est-ce qu'il y a un lien avec ce que vous avez fait?" ; “Les natures mortes, vous vous souvenez ?”). 

---
# Cotation CLASS sur quelques dimensions (2/2)

* **Qualité de la rétroaction \: 6 (+=+++)**. M fournit des informations supplémentaires sur les œuvres (Duchamp), et encourage les E à les réaliser, notamment ceux qui ont des problèmes (étayage). Adresse des rétroactions à tous, projet par projet. 
		
* **Modelage Langagier \: 6 (+++=+)**. M a un niveau de langage élaboré et fait travailler le vocabulaire en lien avec la création des œuvres. La conversation est essentiellement dirigée par M, mais il y a de nombreuses questions ouvertes (“Quelle est la différence ?”) ; et il y a peu ou pas de *self-parallel talk*. De nombreuses répétitions/extensions en lien avec le vocabulaire et questions ouvertes.

---
# Principes pour la suite ! La formation continue !

- Réfléchir régulièrement à sa pratique, demander des avis à ses pairs
- Avoir des “dadas“, des thèmes que vous creusez
- Peaufiner ses méthodes d'enseignement, intégrer le numérique si nécessaire (en faisant attention à la fuite des données personnelles)
- Être curieux.se des résultats de la recherche à propos de ses “dadas”, notamment *via* les journaux professionnels, les blogs, etc.
- Suivre des MOOC sur l'enseignement/apprentissage, s'impliquer dans le projet [PEGASE](https://www.polepilote-pegase.fr), le projet [Ateliers Profs-Chercheurs](https://www.profschercheurs.org/fr)
- Reprendre des études dans quelques années !