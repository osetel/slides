---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Enseigner avec le numérique
## Objectifs, organisation, attendus


<!-- page_number: true -->
<!-- footer: Pôle enseignement numérique INSPE - UGA - 2023 - 2024 - CC:BY-NC-SA -->

---
# Objectifs
Ce module (ou UE) a pour objectif de vous accompagner dans l'acquisition des compétences nécessaires pour utiliser le numérique en classe en prenant en compte les dimensions, efficacité de l’enseignement, respect des normes juridiques et éthiques et développement de la culture numérique des élèves. 

Cette formation articule apports théoriques et activités pratiques aboutissant à la conception puis la mise en œuvre et enfin l’analyse réflexive d’une ou plusieurs situations d’apprentissage intégrant le numérique.

---
# Objectifs
Le travail de conception, de mise en œuvre et d'analyse pourra s’articuler avec un ou des travaux d’autres modules (ou UE) et il devra permettre de répondre aux exigences du dispositif PIX+EDU (nouveauté 2023)

Ce dispositif PIX+EDU comporte deux volets :
* un volet automatisé qui doit être initié à l’INSPE et qui sera finalisé en établissement lors de la première annoée de titularisation.
* un volet pratiques professionnelles réalisé entièrement à l’INSPE

---
# Organisation
**(à adapter en fonction des publics)**

* 3 CM 
* 3 TD 
* 1 TP
:warning: Vous aurez différents intervenants 

---
# Attendus
Conception, mise en œuvre en classe et analyse réflexive d’une situation d'apprentissage intégrant le numérique.
:arrow_right: Rédaction d'un document comportant les éléments de conception, des traces de la mise en oeuvre et le bilan réflexif de la ou des situations d’apprentissages intégrant le numérique au regard des exigences du volet pratiques professionnelles du dispositif PIX+EDU.
:arrow_right: Inscription et positionnement partiel sur les compétences numériques du référentiel CRCNé via la plateforme PIX.

---
# :sos: Ressources
#

#### Cours sur la plateforme eformation :arrow_right:  https://eformation.univ-grenoble-alpes.fr







