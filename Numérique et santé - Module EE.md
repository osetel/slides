---
marp: true
paginate: true
autoscale: true
theme: aqua
---

# Module numérique 1 et 2 EE

![70%](images/img-effets/Effets-logo-nouvelle-uga.png)

## Numérique et santé 

### Christophe Charroud - Univ. Grenoble Alpes
##### Année universitaire 2022-23


<!-- page_number: true -->
<!-- footer:  christophe.charroud@univ-grenoble-alpes.fr – Module numérique 1 et 2 EE – 2022 - CC:BY-NC-SA  -->

---
# Au programme

1. Introduction et composition des équipes
2. Etude d'un sujet par équipe (1h15)
3. Présentation collective des résultats


---

# :one: Introduction et composition des équipes
Durée 5 minutes

---
## Numérique et santé, pourquoi cette question ?
* Problèmes de santé publique
* Informations véhiculées par les médias
* Que dit la recherche ?
* :arrow_right: Rôle du CPE


---
## Constitution de 4 ou 5 équipes
Chaque équipe va réaliser une présentation sur un sujet parmi la liste ci-dessous :
* Les ondes WIFI, 4g, 5g sont-elles nocives pour la santé ?
* Sommeil et écrans, quels liens, quels effets ?
* Écrans et problèmes ophtalmiques, quels liens, quels effets ?
* Numérique et impacts neurologiques (trouble du développement, TDAH, ...) quels liens, quels effets ?
* Numérique et impacts sur l'hygiène de vie (sédentarité, TMS...) quels liens, quels effets ?
* *ou autre sujet présenté par une équipe et validé par l'enseignant...*


---
# :two: :pencil: Recherche documentaire et conception des présentations
:warning: Fin de l'activité à 14h50 

---
## Consignes 

1 - Chaque équipe recherche des documents et des arguments **scientifquement étayés** sur son sujet (références obligatoires).
2 - Chaque équipe prépare une présentation d'une durée de 5 à 6 minutes avec l'appui d'un support visuel.


---

# :three: Présentations 

---

# :four: Débat - Conclusion

