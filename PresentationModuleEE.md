---
marp: true
theme: default 
paginate: true
---

![](images/logo_UGA_couleur_cmjn.jpg)

# Module numérique 1 et 2
## DIU Encadrement Éducatif (EE)

INSPÉ / UGA -2022

<!-- footer: Equipe de formateurs(trices) numérique - Inspé-UGA - 2022 - CC:BY-NC-SA -->

---

# Pourquoi ?

* Pour réduire le plus possible les risques législatifs, éthiques,
physiologiques et effets négatifs liès à l'utilisation du numérique en situation scolaire.
* Pour être capable de participer à la sensibilisation et/ou
l'éducation des élèves, des assistants d'éducation à l'usage des
médias.

---

# Comment

* Via une réflexion personnelle sur une situations scolaires.
* Cette situation sera analysées en s'appuyant sur des apports délivrés au cours de 5 séances thématiques :
  * Numérique et juridique
  * Effets du numérique sur la santé.
  * Les réseaux sociaux : introduction à l’EMI
  * Enseigner avec le numérique : quelques zones à risque
  * thème à définir

---

# Modalité d’évaluation

* Semestre 1 : Assiduité
* Semestre 2 : Analyse réflexive mobilisant les connaissances acquises en formation basée sur un cas d'étude réaliste.

---

# Plan des séances

* **Séances 1 & 5** : Apports thématiques (2h en présence) suivi d’un travail en autonomie (1h à distance) 
* **Séance 6** : Évaluation.

---
# Travail en autonomie

- À la fin de chaque séance un travail en autonomie d'une heure est demandé.

---
# En résumé

* Une formation appuyée sur des apports théoriques et pratiques
* Une évaluation fondée sur une analyse réflexive.

---

# Connexion au cours (1/4)

* Plate-forme : <https://eformation.univ-grenoble-alpes.fr/>
* Connexion par « compte universitaire »

| Page d'accueil | Page de connexion | 
| :-------------: | :---------: | 
|![width:400px](images/img-Intro/Moodle1.png) |![width:350px](images/img-Intro/moodle18.png) |
* Un accès anonyme sera possible pour les premières séances.
---

# Connexion au cours (2/4)

#### Choix de l'établissement

Université Grenoble Alpes ou Université de Savoie ![width:700px](images/img-Intro/Moodle2.png)

Authentification avec ses identifiants universitaires ![width:600px](images/img-Intro/Moodle4.png)

---

# Connexion au cours (3/4)

* Retour à l'accueil du site ![width:300px](images/img-Intro/Moodle5.png)
* Descendre dans la page pour accéder au champ de recherche des cours ![50%](images/img-Intro/Moodle6module.png)
* Sélectionner le cours "Module numérique 1 et 2 EE" 
![width:300px](images/img-Intro/Moodle7EEmodule.png)

---

# Connexion au cours (4/4)

* Vous devez vous auto-inscrire. La clé d’inscription 

![](images/img-Intro/Moodle8EEmodule.png)

* Pour ceux qui n'ont pas encore leurs identifiants universitaires, le mot de passe pour l'accès anomyme est **ModuleNumeriqueEE**

