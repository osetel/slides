---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Effets du numérique sur les apprentissages
## TD
 
Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :zero: Objectifs
### Objectif général de la formation
Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe :arrow_right: Objectif mise en oeuvre.
### Objectifs du TD
* Mettre en pratique les concepts théoriques sur les thématiques
    * Communiquer avec le numérique
    * Entrainement sur des tâches ciblées
* Débuter la conception
 
---

# :one: Ce que l'on sait
Rappels des apports théoriques

## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)

---
#### Il est indispensable d'analyser ses pratiques au regard des résultats de la recherche si l'on veut éviter les problèmes....
# 
###  :warning: Les effets que l'on pense positifs peuvent :warning: rapidement se révéler négatifs :

Outre une perte de temps, d'énergie et de crédibilité: le risque principal est de générer une **augmentation de la différence entre les élèves**
(Zheng, 2016)


---
## Accéder à l'information
### Des points positifs:
* Information disponible tout le temps et en tous lieux (ou presque).
* Quantité d'information 
* Supports variés 

### Des points négatifs:
* Quantité d'information :arrow_right: tri, validité, perte de temps
* Supports variés :arrow_right: nécessite matériels et applications 
* Informations payantes :arrow_right: frustration
* Recherche sur internet :warning: lecture


---
## Communiquer

### Des points positifs :
* Sources variées
* Attention

### Des points négatifs 
* Sources variées
* Attention

# :rage:



---
### :exclamation: À retenir :exclamation:

* Des ressources épurées.
* Des vidéos contrôlables par l'élève, avec des pauses.
* Ne pas oublier que la perception de l'élève est différente de celle de l'adulte (neurones miroirs).
* Et enfin il ne faut pas que l'interface soit un obstacle aux apprentissages

---

### :pencil2: TP : durée 30 minutes
###  Vos documents numériques sont-ils efficaces?
* Regroupez vous par cycle ou niveau de classe en essayant de faire des groupes de 4 ou 5 max.
* Choisissez un ou des documents numériques que vous utilisez en classe et faites en une analyse critique.
* De cette analyse, préparez une présentation de 2 minutes que vous présenterez au reste du groupe TD



---
## S'entrainer sur des tâches ciblées 

### Rappel sur les rétroactions pour l'appentissage
* Nécessité de feedback
* Feedback, si possible, immédiat 
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)

(Dehaene, 2018)

---
# :pencil2: TP : Durée 30 minutes
* En groupe, choisissez un ou des entrainements (vu en classe ou ailleurs) utilisant le numérique.
* Faites-en une analyse critique en insistant sur la qualité du feedback
* De cette analyse, préparez une présentation de 3-4 minutes que vous présenterez au reste du groupe TD

---
# :two: Conception d'une situation d'apprentissage
* Possible de travailler en groupe.
* Production individuelle adaptée à votre enviroennement de classe :arrow_right: mise en oeuvre.
* Vous devrez justifier des choix opérés pour cette conception en vous appuyant sur ce que dit la recherche.



